include(Tools)

find_program(Python_EXECUTABLE NAMES python python2 python3)
assert(Python_EXECUTABLE MESSAGE "python interpreter required")

get_filename_component(NODE_GYP_BINARY_DIR "${CMAKE_SOURCE_DIR}/build" ABSOLUTE)
get_filename_component(CMAKE_BINARY_DIR_ABSOLUTE "${CMAKE_BINARY_DIR}" ABSOLUTE)

assert(NOT "${NODE_GYP_BINARY_DIR}" STREQUAL "${CMAKE_BINARY_DIR_ABSOLUTE}"
  MESSAGE "Do not run cmake in 'build' dir (reserved to node-gyp)")

add_custom_target(npm-install
  COMMAND npm install
  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})

add_custom_target(node-gyp ALL
  DEPENDS npm-install
  COMMAND npm run rebuild
  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})

execute_process(COMMAND node-gyp configure
                WORKING_DIRECTORY  ${PROJECT_SOURCE_DIR})
execute_process(COMMAND "${Python_EXECUTABLE}" -c
  "print(eval(open('${NODE_GYP_BINARY_DIR}/config.gypi').read())['variables']['nodedir'])"
  OUTPUT_VARIABLE NODE_GYP_NODEDIR
  OUTPUT_STRIP_TRAILING_WHITESPACE)

set(NODE_GYP_INCLUDE "${NODE_GYP_NODEDIR}/include/node")
message(STATUS "Node-gyp dir : ${NODE_GYP_INCLUDE}")
