
# Create a linter badge

execute_process(
    COMMAND "${CMAKE_BUILD_TOOL}" lint
    ERROR_VARIABLE LINT_OUT)

string(REGEX REPLACE "[^\n]+" "" LINT_COUNT "${LINT_OUT}")
string(LENGTH "${LINT_COUNT}" LINT_COUNT)

if(LINT_COUNT EQUAL 0)
    set(BADGE "linter-0-brigthgreen")
elseif(LINT_COUNT LESS 6)
    set(BADGE "linter-${LINT_COUNT}-yellow")
elseif(LINT_COUNT LESS 11)
    set(BADGE "linter-${LINT_COUNT}-orange")
else()
    set(BADGE "linter-${LINT_COUNT}-red")
endif()

file(DOWNLOAD "https://img.shields.io/badge/${BADGE}.svg" "${CMAKE_CURRENT_BINARY_DIR}/lint_badge.svg")
message(STATUS "Lint badge generated !")
