/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#ifndef RBAC_CONFIGURATION_H_
#define RBAC_CONFIGURATION_H_

#include <algorithm>
#include <functional>
#include <memory>
#include <mutex>

#include <cmw-fwk/client/rbac/RbacLoginService.h>
#include <node_api.h>

#include "NAPIUtils.hpp"

class RBACConfiguration : public rbac::AuthenticationListener
{
public:
    static napi_value Init(napi_env env, napi_value exports);
    static void Destructor(napi_env env,
                           void *nativeObject,
                           void *finalize_hint);
    static napi_status NewInstance(napi_env env, napi_value *instance);

    static napi_value GetConstructor(napi_env env);
    static napi_value GetLoginPolicy(napi_env env, napi_callback_info info);
    static napi_value SetLoginPolicy(napi_env env, napi_callback_info info);
    static napi_value SetLoginName(napi_env env, napi_callback_info info);
    static napi_value GetLoginName(napi_env env, napi_callback_info info);
    static napi_value SetPassword(napi_env env, napi_callback_info info);
    static napi_value SetApplicationName(napi_env env, napi_callback_info info);
    static napi_value GetApplicationName(napi_env env, napi_callback_info info);
    static napi_value SetAuthDoneCB(napi_env env, napi_callback_info info);
    static napi_value SetAuthErrorCB(napi_env env, napi_callback_info info);
    static napi_value SetAuthExpiredCB(napi_env env, napi_callback_info info);

    /**
     * Gets reference to the client service
     * configured with this RBAC configuration
     */
    cmw::rda3::client::ClientService *getClientService();

    void authenticationDone(const rbac::Token &token) override;
    void authenticationError(const rbac::AuthenticationFailure &error) override;
    void tokenExpired(const rbac::Token &token) override;

private:
    RBACConfiguration();
    ~RBACConfiguration();
    void Delete(napi_env env);

protected:
    void reconfigure();

    static napi_ref s_constructor;
    static napi_value New(napi_env env, napi_callback_info info);
    static void authChangedJSReplyCallback(napi_env env,
                                           napi_value js_callback,
                                           void *context,
                                           void *vdata);
    static napi_value createToken(napi_env env, const rbac::Token &token);

    napi_ref m_wrapper;
    rbac::LoginPolicy m_loginPolicy;
    bool m_cfgNeeded;
    std::string m_userName;
    std::string m_password;
    std::string m_applicationName;
    napi_threadsafe_function m_authChangedFunc;
    napi_ref m_authDoneJSFunc;
    napi_ref m_authErrorJSFunc;
    napi_ref m_authExpiredJSFunc;
    std::unique_ptr<cmw::rda3::client::ClientService> m_clientSvc;
    std::unique_ptr<cmw::RbacLoginService> m_rbacLoginSvc;
    std::mutex m_mutex;
};

#endif // DATA_H_
