/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#include "AsyncSetter.hpp"

#include <cassert>
#include <iostream>

#include "NAPIUtils.hpp"

using namespace cmw::rda3;

napi_value AsyncSetter::NewSetterPromise(
    napi_env env,
    AccessPoint *ap,
    std::unique_ptr<cmw::data::Data> data,
    const std::string &selector,
    std::unique_ptr<cmw::data::Data> context_data)
{
    napi_value work_name, promise;
    AsyncSetter *theSetter = new AsyncSetter(env, ap, data, selector,
                                             context_data);

    // Create a string to describe this asynchronous operation.
    NAPI_CALL(napi_create_string_utf8(env, "RDA Setter", NAPI_AUTO_LENGTH,
                                      &work_name));

    // Create a deferred promise which we will resolve at the completion of the
    // work.
    NAPI_CALL(napi_create_promise(env, &(theSetter->m_deferred), &promise));

    // Create an async work item, passing in the addon data, which will give the
    // worker thread access to the above-created deferred promise.
    NAPI_CALL(napi_create_async_work(
        env, NULL, work_name, AsyncSetter::ExecuteTask,
        AsyncSetter::TaskCompleted, theSetter, &(theSetter->m_worker)));

    // Queue the work item for execution.
    NAPI_CALL(napi_queue_async_work(env, theSetter->m_worker));

    // This causes created `promise` to be returned to JavaScript.
    return promise;
}

void AsyncSetter::Delete(napi_env env)
{
    // Clean up the work item associated with this run.
    NAPI_CALL(napi_delete_async_work(env, m_worker));

    m_ap->removeReference(env);
    m_ap = nullptr;

    delete this;
}

AsyncSetter::AsyncSetter(napi_env env,
                         AccessPoint *ap,
                         std::unique_ptr<cmw::data::Data> &data,
                         const std::string &selector,
                         std::unique_ptr<cmw::data::Data> &contextData) :
    m_ap(ap),
    m_data(std::move(data)),
    m_contextData(std::move(contextData)),
    m_selector(selector)
{
    m_ap->addReference(env);
}

AsyncSetter::~AsyncSetter()
{
    /* AsyncSetter::Delete should release the Node part */
    assert(m_ap == nullptr);
}

void AsyncSetter::ExecuteTask(napi_env /* env */, void *data)
{
    AsyncSetter *setter = reinterpret_cast<AsyncSetter *>(data);
    try
    {
        std::unique_ptr<::common::RequestContext> rcontext{
            ::common::RequestContextFactory::create(
                setter->m_selector, std::unique_ptr<cmw::data::Data>(),
                std::move(setter->m_contextData))};
        ::client::AccessPoint &accessPoint = setter->m_ap->getAccessPoint();

        accessPoint.set(std::move(setter->m_data), std::move(rcontext));
    }
    catch (const std::exception &e)
    {
        setter->m_error.reset(new std::string(e.what()));
    }
}

void AsyncSetter::TaskCompleted(napi_env env, napi_status status, void *data)
{
    AsyncSetter *setter = reinterpret_cast<AsyncSetter *>(data);
    if (status != napi_ok)
    {
        setter->Delete(env);
        return;
    }

    if (setter->m_error == nullptr)
    {
        napi_value doneMsg;
        NAPI_CALL(
            napi_create_string_utf8(env, "done", NAPI_AUTO_LENGTH, &doneMsg));
        NAPI_CALL(napi_resolve_deferred(env, setter->m_deferred, doneMsg));
    }
    else
    {
        // Exception occured
        napi_value errMsg;
        NAPI_CALL(napi_create_string_utf8(env, setter->m_error->c_str(),
                                          NAPI_AUTO_LENGTH, &errMsg));
        NAPI_CALL(napi_reject_deferred(env, setter->m_deferred, errMsg));
    }

    setter->Delete(env);
}