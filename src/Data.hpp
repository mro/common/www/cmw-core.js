/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#ifndef DATA_H_
#define DATA_H_

#include <algorithm>
#include <functional>
#include <memory>

#include <cmw-data/Data.h>
#include <node_api.h>

#include "NAPIUtils.hpp"

class Data
{
public:
    static napi_value Init(napi_env env, napi_value exports);
    static void Destructor(napi_env env,
                           void *nativeObject,
                           void *finalize_hint);
    static napi_status NewInstance(napi_env env, napi_value *instance);

    static napi_value NewInstance(napi_env env, const cmw::data::Data &data);
    static napi_value GetConstructor(napi_env env);

    std::unique_ptr<cmw::data::Data> cloneData() const;

private:
    Data();
    ~Data();
    void Delete(napi_env env);

protected:
    enum NumberType
    {
        Double = 0,
        Float,
        Int64,
        Int32,
        Int16,
        Int8
    };
    struct NumberTypeValues
    {
        const char *name;
        NumberType type;
    };
    static constexpr NumberTypeValues s_numberTypes[] = {
        {"Double", Double}, {"Float", Float}, {"Int64", Int64},
        {"Int32", Int32},   {"Int16", Int16}, {"Int8", Int8}};

    enum BigIntModeType
    {
        BigInt,
        String,
        Timestamp
    };
    struct BigIntModeValue
    {
        const char *name;
        BigIntModeType type;
    };
    static constexpr BigIntModeValue s_bigIntOutputTypes[] = {
        {"BigInt", BigInt},
        {"String", String},
        {"Timestamp", Timestamp}};

    enum NumberArrayModeType
    {
        Regular,
        TypedArray
    };
    struct NumberArrayModeValue
    {
        const char *name;
        NumberArrayModeType type;
    };
    static constexpr NumberArrayModeValue s_numberArrayModeTypes[] = {
        {"Regular", Regular},
        {"TypedArray", TypedArray}};

    static napi_ref s_constructor;
    static const BigIntModeValue *getBigIntModeValue(int32_t type);
    static const BigIntModeValue *getBigIntModeValue(const std::string &type);
    static const NumberArrayModeValue *getNumberArrayModeValue(int32_t type);
    static const NumberArrayModeValue *getNumberArrayModeValue(
        const std::string &type);

    static napi_value New(napi_env env, napi_callback_info info);
    static napi_value Append(napi_env env, napi_callback_info info);
    static napi_status AppendTypedArray(napi_env env,
                                        napi_value value,
                                        const std::string &name,
                                        Data *obj);
    static napi_status AppendArray(napi_env env,
                                   napi_value value,
                                   const std::string &name,
                                   Data *obj,
                                   NumberType type,
                                   bool interpretNumber);
    static napi_status AppendFunction(napi_env env,
                                      napi_value value,
                                      const std::string &name,
                                      Data *obj);
    static napi_status AppendTyped2DArray(napi_env env,
                                          napi_value value,
                                          const std::string &name,
                                          Data *obj);
    static napi_status Append2DArray(napi_env env,
                                     napi_value value,
                                     const std::string &name,
                                     Data *obj,
                                     NumberType type,
                                     bool interpretNumber);
    static napi_value Entries(napi_env env, napi_callback_info info);
    static napi_value GetDataType(napi_env env, napi_callback_info info);
    static napi_value SetBigIntMode(napi_env env, napi_callback_info info);
    static napi_value GetBigIntMode(napi_env env, napi_callback_info info);
    static napi_value SetNumberArrayMode(napi_env env, napi_callback_info info);
    static napi_value GetNumberArrayMode(napi_env env, napi_callback_info info);
    static napi_value DeleteEntry(napi_env env, napi_callback_info info);
    static napi_value ClearEntries(napi_env env, napi_callback_info info);

    template<typename T>
    inline static napi_status CreateJSNumber(napi_env env,
                                             T value,
                                             napi_value *result)
    {
        napi_status ret;
        ret = napi_create_int32(env, static_cast<int32_t>(value), result);
        return ret;
    }

    static napi_status CreateJSFunction(
        napi_env env,
        Data *obj,
        const cmw::data::DiscreteFunction &function,
        napi_value *result);

    template<typename T>
    inline static napi_status CreateJSArray(napi_env env,
                                            Data *obj,
                                            const T *arrValue,
                                            const size_t &arrSize,
                                            napi_typedarray_type type,
                                            napi_value *result)
    {
        napi_status ret;
        if (obj->m_numberArrayMode->type == TypedArray)
        {
            napi_value buffer;
            void *output_ptr = NULL;

            ret = napi_create_arraybuffer(env, arrSize * sizeof(T), &output_ptr,
                                          &buffer);
            if (ret != napi_ok)
                return ret;

            T *output_typed = (T *) output_ptr;
            std::copy_n(arrValue, arrSize, output_typed);
            ret = napi_create_typedarray(env, type, arrSize, buffer, 0, result);
        }
        else
        {
            ret = napi_create_array(env, result);
            if (ret != napi_ok)
                return ret;
            for (size_t i = 0; i < arrSize; i++)
            {
                napi_value cellVal;
                ret = CreateJSNumber(env, arrValue[i], &cellVal);
                if (ret != napi_ok)
                    return ret;
                ret = napi_set_element(env, *result, i, cellVal);
                if (ret != napi_ok)
                    return ret;
            }
        }
        return ret;
    }

    inline static napi_status CreateJSArray(napi_env env,
                                            Data * /* obj */,
                                            const char **arrValue,
                                            const size_t &arrSize,
                                            napi_value *result)
    {
        napi_status ret;
        ret = napi_create_array(env, result);
        if (ret != napi_ok)
            return ret;
        for (size_t i = 0; i < arrSize; i++)
        {
            napi_value strVal;
            ret = napi_create_string_utf8(env, arrValue[i], NAPI_AUTO_LENGTH,
                                          &strVal);
            if (ret != napi_ok)
                return ret;
            ret = napi_set_element(env, *result, i, strVal);
            if (ret != napi_ok)
                return ret;
        }
        return ret;
    }

    template<typename T>
    inline static napi_status CreateJS2DArray(napi_env env,
                                              Data *obj,
                                              const T *arrValue,
                                              const size_t &rowCount,
                                              const size_t &colCount,
                                              napi_typedarray_type type,
                                              napi_value *result)
    {
        napi_status ret;
        ret = napi_create_array(env, result);
        if (ret != napi_ok)
            return ret;
        const T *ptr = arrValue;
        for (size_t i = 0; i < rowCount; i++)
        {
            napi_value val;
            ret = CreateJSArray(env, obj, ptr, colCount, type, &val);
            if (ret != napi_ok)
                return ret;
            ret = napi_set_element(env, *result, i, val);
            if (ret != napi_ok)
                return ret;
            ptr += colCount;
        }
        return ret;
    }

    inline static napi_status CreateJS2DArray(napi_env env,
                                              Data *obj,
                                              const char **arrValue,
                                              const size_t &rowCount,
                                              const size_t &colCount,
                                              napi_value *result)
    {
        napi_status ret;
        ret = napi_create_array(env, result);
        if (ret != napi_ok)
            return ret;
        const char **ptr = arrValue;
        for (size_t i = 0; i < rowCount; i++)
        {
            napi_value val;
            ret = CreateJSArray(env, obj, ptr, colCount, &val);
            if (ret != napi_ok)
                return ret;
            ret = napi_set_element(env, *result, i, val);
            if (ret != napi_ok)
                return ret;
            ptr += colCount;
        }
        return ret;
    }

    template<typename T>
    static inline napi_status ApppendArray(
        napi_env env,
        Data *obj,
        const std::string &name,
        napi_value value,
        napi_valuetype firstCellType,
        std::function<napi_status(napi_env, T *, napi_value)> setter,
        uint32_t rowCount)
    {
        napi_status res;
        std::unique_ptr<T[]> array(new T[rowCount]);
        for (uint32_t i = 0; i < rowCount; ++i)
        {
            napi_value cellValue;
            res = napi_get_element(env, value, i, &cellValue);
            if (res != napi_ok)
                return res;
            napi_valuetype cellType;
            res = napi_typeof(env, cellValue, &cellType);
            if (res != napi_ok)
                return res;
            if (cellType != firstCellType)
            {
                napi_throw_error(env, "EINVAL",
                                 "Array elements must have same type");
                return napi_invalid_arg;
            }
            res = setter(env, &array[i], cellValue);
            if (res != napi_ok)
                return res;
        }
        obj->m_data->appendArray(name, array.get(), rowCount);
        return res;
    }

    template<typename T>
    static inline napi_status Append2DTypedArray(
        napi_env env,
        Data *obj,
        const std::string &name,
        napi_typedarray_type expectedType,
        napi_value value,
        uint32_t rowCount,
        uint32_t colCount)
    {
        napi_status res;
        std::unique_ptr<T[]> array(new T[rowCount * colCount]);
        T *target = array.get();
        for (uint32_t i = 0; i < rowCount; ++i)
        {
            napi_value jsVal;
            res = napi_get_element(env, value, i, &jsVal);
            if (res != napi_ok)
                return res;
            size_t elemArrLen, byte_offset;
            napi_typedarray_type elemArrType;
            void *data;
            napi_value arrayBuffer;
            res = napi_get_typedarray_info(env, jsVal, &elemArrType, &elemArrLen,
                                           &data, &arrayBuffer, &byte_offset);
            if (res != napi_ok)
                return res;
            if (expectedType != elemArrType)
            {
                napi_throw_error(env, "ENINVAL",
                                 "TypedArray must have same type");
                return napi_invalid_arg;
            }
            if (colCount != elemArrLen)
            {
                napi_throw_error(env, "ENINVAL",
                                 "2D arrays must have same size");
                return napi_invalid_arg;
            }
            T *ptr = static_cast<T *>(data);
            std::copy(ptr, ptr + elemArrLen, target);
            target += elemArrLen;
        }
        obj->m_data->appendArray2D(name, array.get(), rowCount, colCount);
        return res;
    }

    template<typename T>
    static inline napi_status Apppend2DArray(
        napi_env env,
        Data *obj,
        const std::string &name,
        napi_value value,
        napi_valuetype firstCellType,
        std::function<napi_status(napi_env, T *, napi_value)> setter,
        uint32_t rowCount,
        uint32_t colCount)
    {
        napi_status res;
        std::unique_ptr<T[]> array(new T[rowCount * colCount]);
        T *ptr = array.get();
        for (uint32_t i = 0; i < rowCount; ++i)
        {
            napi_value jsArray;
            res = napi_get_element(env, value, i, &jsArray);
            if (res != napi_ok)
                return res;
            uint32_t arrLen;
            res = napi_get_array_length(env, jsArray, &arrLen);
            if (res != napi_ok)
                return res;
            if (arrLen != colCount)
            {
                napi_throw_error(env, "EINVAL", "2D arrays must have same size");
                return napi_invalid_arg;
            }
            for (uint32_t j = 0; j < arrLen; ++j)
            {
                napi_value cellValue;
                res = napi_get_element(env, jsArray, j, &cellValue);
                if (res != napi_ok)
                    return res;
                napi_valuetype cellType;
                res = napi_typeof(env, cellValue, &cellType);
                if (res != napi_ok)
                    return res;
                if (cellType != firstCellType)
                {
                    napi_throw_error(env, "EINVAL",
                                     "Array elements must have same type");
                    return napi_invalid_arg;
                }
                res = setter(env, ptr, cellValue);
                if (res != napi_ok)
                    return res;
                ++ptr;
            }
        }
        obj->m_data->appendArray2D(name, array.get(), rowCount, colCount);
        return res;
    }

    inline static void AppendNumberFromString(Data *obj,
                                              const std::string &strVal,
                                              const std::string &name,
                                              NumberType numberType)
    {
        switch (numberType)
        {
        case Double: obj->m_data->append(name, std::stod(strVal)); break;
        case Float: obj->m_data->append(name, std::stof(strVal)); break;
        case Int64:
            obj->m_data->append(name, static_cast<int64_t>(std::stoll(strVal)));
            break;
        case Int32:
            obj->m_data->append(name, static_cast<int32_t>(std::stol(strVal)));
            break;
        case Int16:
            obj->m_data->append(name, static_cast<int16_t>(std::stol(strVal)));
            break;
        case Int8:
            obj->m_data->append(name, static_cast<int8_t>(std::stol(strVal)));
            break;
        }
    }

    napi_ref m_wrapper;
    napi_ref m_jsData;
    const BigIntModeValue *m_int64Mode;
    const NumberArrayModeValue *m_numberArrayMode;
    std::unique_ptr<cmw::data::Data> m_data;
    std::function<napi_status(napi_env, int64_t, napi_value *)> m_createInt64Js;
};

template<>
inline napi_status Data::CreateJSNumber(napi_env env,
                                        float value,
                                        napi_value *result)
{
    napi_status ret;
    ret = napi_create_double(env, static_cast<double>(value), result);
    return ret;
}

template<>
inline napi_status Data::CreateJSNumber(napi_env env,
                                        double value,
                                        napi_value *result)
{
    napi_status ret;
    ret = napi_create_double(env, value, result);
    return ret;
}

// Specialization for boolean entries
template<>
inline napi_status Data::CreateJSArray(napi_env env,
                                       Data * /* obj */,
                                       const bool *arrValue,
                                       const size_t &arrSize,
                                       napi_typedarray_type /* type */,
                                       napi_value *result)
{
    napi_status ret;
    ret = napi_create_array(env, result);
    if (ret != napi_ok)
        return ret;
    for (size_t i = 0; i < arrSize; i++)
    {
        napi_value bVal;
        ret = napi_get_boolean(env, arrValue[i], &bVal);
        if (ret != napi_ok)
            return ret;
        ret = napi_set_element(env, *result, i, bVal);
        if (ret != napi_ok)
            return ret;
    }
    return ret;
}

// Specialization for long entries
template<>
inline napi_status Data::CreateJSArray(napi_env env,
                                       Data *obj,
                                       const int64_t *arrValue,
                                       const size_t &arrSize,
                                       napi_typedarray_type /* type */,
                                       napi_value *result)
{
    napi_status ret;
    ret = napi_create_array(env, result);
    if (ret != napi_ok)
        return ret;
    for (size_t i = 0; i < arrSize; i++)
    {
        napi_value jsVal;
        ret = obj->m_createInt64Js(env, arrValue[i], &jsVal);
        if (ret != napi_ok)
            return ret;
        ret = napi_set_element(env, *result, i, jsVal);
        if (ret != napi_ok)
            return ret;
    }
    return ret;
}

#endif // DATA_H_
