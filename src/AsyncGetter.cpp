/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#include "AsyncGetter.hpp"

#include <cassert>
#include <iostream>

#include "AcquiredData.hpp"
#include "NAPIUtils.hpp"

using namespace cmw::rda3;

napi_value AsyncGetter::NewGetterPromise(
    napi_env env,
    AccessPoint *ap,
    const std::string &selector,
    std::unique_ptr<cmw::data::Data> context_data)
{
    napi_value work_name, promise;
    AsyncGetter *theGetter = new AsyncGetter(env, ap, selector, context_data);

    // Create a string to describe this asynchronous operation.
    NAPI_CALL(napi_create_string_utf8(env, "RDA getter", NAPI_AUTO_LENGTH,
                                      &work_name));

    // Create a deferred promise which we will resolve at the completion of the
    // work.
    NAPI_CALL(napi_create_promise(env, &(theGetter->m_deferred), &promise));

    // Create an async work item, passing in the addon data, which will give the
    // worker thread access to the above-created deferred promise.
    NAPI_CALL(napi_create_async_work(
        env, NULL, work_name, AsyncGetter::ExecuteTask,
        AsyncGetter::TaskCompleted, theGetter, &(theGetter->m_worker)));

    // Queue the work item for execution.
    NAPI_CALL(napi_queue_async_work(env, theGetter->m_worker));

    // This causes created `promise` to be returned to JavaScript.
    return promise;
}

void AsyncGetter::Delete(napi_env env)
{
    NAPI_CALL(napi_delete_async_work(env, m_worker));

    // We don't need the access point anymore
    m_ap->removeReference(env);
    m_ap = nullptr;

    delete this;
}

AsyncGetter::AsyncGetter(napi_env env,
                         AccessPoint *ap,
                         const std::string &selector,
                         std::unique_ptr<cmw::data::Data> &context_data) :
    m_ap(ap), m_contextData(std::move(context_data)), m_selector(selector)
{
    // Registers a reference to the access point
    m_ap->addReference(env);
}

AsyncGetter::~AsyncGetter()
{
    /* AsyncSetter::Delete should release the Node part */
    assert(m_ap == nullptr);
}

void AsyncGetter::ExecuteTask(napi_env /* env */, void *data)
{
    AsyncGetter *getter = reinterpret_cast<AsyncGetter *>(data);
    try
    {
        std::unique_ptr<::common::RequestContext> rcontext{
            ::common::RequestContextFactory::create(
                getter->m_selector, std::unique_ptr<cmw::data::Data>(),
                std::move(getter->m_contextData))};
        ::client::AccessPoint &accessPoint = getter->m_ap->getAccessPoint();

        getter->m_data = accessPoint.get(std::move(rcontext));
    }
    catch (const std::exception &e)
    {
        getter->m_data.reset();
        getter->m_error = e.what();
    }
}

void AsyncGetter::TaskCompleted(napi_env env, napi_status status, void *data)
{
    AsyncGetter *getter = reinterpret_cast<AsyncGetter *>(data);
    if (status != napi_ok)
    {
        getter->Delete(env);
        return;
    }

    if (getter->m_data.get())
    {
        napi_value rdaData = AcquiredData::NewInstance(env, getter->m_data);
        NAPI_CALL(napi_resolve_deferred(env, getter->m_deferred, rdaData));
    }
    else
    {
        // Exception occured
        napi_value errMsg;
        NAPI_CALL(napi_create_string_utf8(env, getter->m_error.c_str(),
                                          NAPI_AUTO_LENGTH, &errMsg));
        NAPI_CALL(napi_reject_deferred(env, getter->m_deferred, errMsg));
    }

    getter->Delete(env);
}