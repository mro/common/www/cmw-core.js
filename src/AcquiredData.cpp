/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#include "AcquiredData.hpp"

#include <cassert>
#include <iostream>

#include "Data.hpp"
#include "NAPIUtils.hpp"

using namespace cmw::rda3;

AcquiredData::AcquiredData() :
    m_wrapper(nullptr), m_cycleStamp(0), m_acqStamp(0), m_data(nullptr)
{}

AcquiredData::~AcquiredData()
{
    // use AcquiredData::Delete
    assert(m_wrapper == nullptr);
    assert(m_data == nullptr);
}

static void DestructorNoGC(node_api_nogc_env env, void *data, void *hint)
{
#if defined(NAPI_EXPERIMENTAL)
    node_api_post_finalizer(env, AcquiredData::Destructor, data, hint);
#else
    AcquiredData::Destructor(env, data, hint);
#endif
}

void AcquiredData::Destructor(napi_env env,
                              void *nativeObject,
                              void * /*finalize_hint*/)
{
    reinterpret_cast<AcquiredData *>(nativeObject)->Delete(env);
}

napi_ref AcquiredData::s_constructor = nullptr;

napi_value AcquiredData::Init(napi_env env)
{
    napi_value cons;
    NAPI_CALL(napi_define_class(env, "AcquiredData", NAPI_AUTO_LENGTH, New,
                                nullptr, 0, nullptr, &cons));

    NAPI_CALL(napi_create_reference(env, cons, 1, &s_constructor));

    return cons;
}

napi_value AcquiredData::New(napi_env env, napi_callback_info info)
{
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, nullptr, nullptr, &jsthis, nullptr));

    AcquiredData *obj = new AcquiredData();
    NAPI_CALL(napi_wrap(env, jsthis, reinterpret_cast<void *>(obj),
                        DestructorNoGC, nullptr, /* finalize_hint */
                        &obj->m_wrapper));

    napi_property_descriptor properties[] = {
        DECLARE_NAPI_GETTER_SETTER_ATTR("cycleName", AcquiredData::GetCycleName,
                                        AcquiredData::SetCycleName,
                                        napi_enumerable),
        DECLARE_NAPI_GETTER_SETTER_ATTR(
            "cycleStamp", AcquiredData::GetCycleStamp,
            AcquiredData::SetCycleStamp, napi_enumerable),
        DECLARE_NAPI_GETTER_SETTER_ATTR("acqStamp", AcquiredData::GetAcqStamp,
                                        AcquiredData::SetAcqStamp,
                                        napi_enumerable),
        DECLARE_NAPI_GETTER_SETTER_ATTR("data", AcquiredData::GetData,
                                        AcquiredData::SetData, napi_enumerable)};

    NAPI_CALL(napi_define_properties(
        env, jsthis, sizeof(properties) / sizeof(napi_property_descriptor),
        properties));
    return jsthis;
}

void AcquiredData::Delete(napi_env env)
{
    if (m_wrapper)
    {
        napi_delete_reference(env, m_wrapper);
        m_wrapper = nullptr;
    }

    if (m_data)
    {
        napi_delete_reference(env, m_data);
        m_data = nullptr;
    }

    delete this;
}

napi_value AcquiredData::NewInstance(
    napi_env env,
    const std::unique_ptr<::common::AcquiredData> &data)
{
    napi_value instance;

    if (!s_constructor)
        AcquiredData::Init(env);

    napi_value cons;
    NAPI_CALL(napi_get_reference_value(env, s_constructor, &cons));

    NAPI_CALL(napi_new_instance(env, cons, 0, nullptr, &instance));

    AcquiredData *obj;
    NAPI_CALL(napi_unwrap(env, instance, reinterpret_cast<void **>(&obj)));

    // Define properties for this object
    const ::common::AcquiredContext &acqContext = data->getContext();

    obj->m_acqStamp = acqContext.getAcqStamp();
    obj->m_cycleName = acqContext.getCycleName();
    obj->m_cycleStamp = acqContext.getCycleStamp();

    // Acquisition data
    napi_value theData;
    theData = Data::NewInstance(env, data->getData());
    NAPI_CALL(napi_create_reference(env, theData, 1, &obj->m_data));
    return instance;
}

napi_value AcquiredData::GetConstructor(napi_env env)
{
    napi_value ret = NULL;
    NAPI_CALL(napi_get_reference_value(env, s_constructor, &ret));
    return ret;
}

napi_value AcquiredData::GetCycleName(napi_env env, napi_callback_info info)
{
    napi_value ret;
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, 0, nullptr, &jsthis, nullptr));

    AcquiredData *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    NAPI_CALL(napi_create_string_utf8(env, obj->m_cycleName.c_str(),
                                      NAPI_AUTO_LENGTH, &ret));

    return ret;
}

napi_value AcquiredData::SetCycleName(napi_env env, napi_callback_info info)
{
    napi_value ret;
    napi_value jsthis;
    napi_value args[1];
    size_t argc = 1;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, args, &jsthis, nullptr));

    AcquiredData *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    if (NAPIUtils::getUTF8String(env, obj->m_cycleName, args[0]) != napi_ok)
    {
        NAPI_CALL(napi_throw_error(env, "EINVAL", "Bad argument"));
    }

    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value AcquiredData::GetCycleStamp(napi_env env, napi_callback_info info)
{
    napi_value ret;
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, 0, nullptr, &jsthis, nullptr));

    AcquiredData *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    NAPI_CALL(napi_create_bigint_int64(env, obj->m_cycleStamp, &ret));
    return ret;
}

napi_value AcquiredData::SetCycleStamp(napi_env env, napi_callback_info info)
{
    napi_value ret;
    napi_value jsthis;
    napi_value argv[1];
    size_t argc = 1;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    AcquiredData *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    bool lossless;
    if (napi_get_value_bigint_int64(env, argv[0], &obj->m_cycleStamp,
                                    &lossless) != napi_ok)
    {
        NAPI_CALL(napi_throw_error(env, "EINVAL", "Bad argument"));
    }

    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value AcquiredData::GetAcqStamp(napi_env env, napi_callback_info info)
{
    napi_value ret;
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, 0, nullptr, &jsthis, nullptr));

    AcquiredData *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    NAPI_CALL(napi_create_bigint_int64(env, obj->m_acqStamp, &ret));
    return ret;
}

napi_value AcquiredData::SetAcqStamp(napi_env env, napi_callback_info info)
{
    napi_value ret;
    napi_value jsthis;
    napi_value argv[1];
    size_t argc = 1;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    AcquiredData *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    bool lossless;
    if (napi_get_value_bigint_int64(env, argv[0], &obj->m_acqStamp,
                                    &lossless) != napi_ok)
    {
        NAPI_CALL(napi_throw_error(env, "EINVAL", "Bad argument"));
    }

    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value AcquiredData::GetData(napi_env env, napi_callback_info info)
{
    napi_value ret;
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, 0, nullptr, &jsthis, nullptr));

    AcquiredData *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    if (obj->m_data)
    {
        NAPI_CALL(napi_get_reference_value(env, obj->m_data, &ret));
    }
    else
    {
        NAPI_CALL(napi_get_undefined(env, &ret));
    }
    return ret;
}

napi_value AcquiredData::SetData(napi_env env, napi_callback_info info)
{
    napi_value ret;
    napi_value jsthis;
    napi_value argv[1];
    size_t argc = 1;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    NAPI_CALL(napi_get_undefined(env, &ret));

    AcquiredData *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    bool is_instance = false;
    NAPI_CALL(
        napi_instanceof(env, argv[0], Data::GetConstructor(env), &is_instance));
    if (!is_instance)
    {
        napi_throw_error(env, "EINVAL", "Argument is not a Data object");
        return ret;
    }
    if (obj->m_data)
    {
        NAPI_CALL(napi_delete_reference(env, obj->m_data));
    }

    NAPI_CALL(napi_create_reference(env, argv[0], 1, &obj->m_data));
    return ret;
}

const std::string &AcquiredData::getCycleName() const
{
    return m_cycleName;
}

int64_t AcquiredData::getCycleStamp() const
{
    return m_cycleStamp;
}

int64_t AcquiredData::getAcqStamp() const
{
    return m_acqStamp;
}

const Data *AcquiredData::getData(napi_env env) const
{
    napi_value data;
    NAPI_CALL(napi_get_reference_value(env, m_data, &data));
    Data *obj;
    NAPI_CALL(napi_unwrap(env, data, reinterpret_cast<void **>(&obj)));
    return obj;
}

std::unique_ptr<::common::AcquiredContext> AcquiredData::getContext() const
{
    return std::unique_ptr<::common::AcquiredContext>(
        new ::AcquiredContext(this));
}