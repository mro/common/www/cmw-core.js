import { expectType } from 'tsd';

import { AccessPoint, Server, AcquiredData, Data } from '.';

(async function() {

  const data = new AcquiredData();
  data.cycleName = "MyCycle";
  data.cycleStamp = BigInt(0);
  data.acqStamp = BigInt(0);
  data.data = new Data();
  data.data.append('msg', 42);

  const server = new Server("test");
  server.setSubscribeCallback((req) => {
    expectType<Server.Request>(req);
    return data;
  });
  server.setSubSrcAddedCallback((req) => {
    expectType<Server.SubscriptionSource>(req);
  });
  server.setSubSrcRemovedCallback((req) => {
    expectType<Server.SubscriptionSource>(req);
  });
  server.setGetCallback((req) => {
    expectType<Server.Request>(req);
  });

  const ap = new AccessPoint('dev', 'prop', 'test');
  ap.subscribe();
  ap.subscribe('selector');
  expectType<AcquiredData>(await ap.get());
  
  await ap.set(new Data());
  expectType<Data>(await ap.createData());
}());