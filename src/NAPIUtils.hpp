/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#ifndef NAPI_UTILS_H_
#define NAPI_UTILS_H_

#include <cctype>
#include <string>

#include <node_api.h>

/**
typedef struct {
  // One of utf8name or name should be NULL.
  const char* utf8name;
  napi_value name;

  napi_callback method;
  napi_callback getter;
  napi_callback setter;
  napi_value value;

  napi_property_attributes attributes;
  void* data;
} napi_property_descriptor;
*/

#define DECLARE_NAPI_GETTER(name, func)         \
    {                                           \
        name, 0, 0, func, 0, 0, napi_default, 0 \
    }

#define DECLARE_NAPI_GETTER_ATTR(name, func, attributes) \
    {                                                    \
        name, 0, 0, func, 0, 0, attributes, 0            \
    }

#define DECLARE_NAPI_SETTER(name, func)         \
    {                                           \
        name, 0, 0, 0, func, 0, napi_default, 0 \
    }

#define DECLARE_NAPI_GETTER_SETTER_ATTR(name, getter, setter, attributes) \
    {                                                                     \
        name, 0, 0, getter, setter, 0,                                    \
            static_cast<napi_property_attributes>(attributes), 0          \
    }

#define DECLARE_NAPI_METHOD(name, func)         \
    {                                           \
        name, 0, func, 0, 0, 0, napi_default, 0 \
    }

#define DECLARE_NAPI_STATIC_METHOD(name, func) \
    {                                          \
        name, 0, func, 0, 0, 0, napi_static, 0 \
    }

#define DECLARE_NAPI_VALUE(name, value)          \
    {                                            \
        name, 0, 0, 0, 0, value, napi_default, 0 \
    }

#define DECLARE_NAPI_VALUE_ATTR(name, value, attributes) \
    {                                                    \
        name, 0, 0, 0, 0, value, attributes, 0           \
    }

#define NAPI_CALL(call) assert(call == napi_ok)

class NAPIUtils
{
public:
    static napi_status getUTF8String(napi_env env,
                                     std::string &str,
                                     napi_value value);
    static napi_status int64ToJSString(napi_env env,
                                       int64_t value,
                                       napi_value *result);
    static napi_status int64ToTimeStamp(napi_env env,
                                        int64_t value,
                                        napi_value *result);
    static napi_status addPropertyToObject(napi_env env,
                                           const std::string &val,
                                           const std::string &name,
                                           napi_value obj);
    static napi_status addPropertyToObject(napi_env env,
                                           int64_t val,
                                           const std::string &name,
                                           napi_value obj);
    static bool caseInsensCompare(const std::string &str1,
                                  const std::string &str2);

private:
    NAPIUtils() = delete;
    virtual ~NAPIUtils() = delete;
};

#endif