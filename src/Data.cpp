/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#include "Data.hpp"

#include <cassert>
#include <iostream>

#include <cmw-data/DataExceptions.h>
#include <cmw-data/DataFactory.h>

#include "NAPIUtils.hpp"

Data::Data() :
    m_wrapper(nullptr),
    m_jsData(nullptr),
    m_int64Mode(&s_bigIntOutputTypes[0]),
    m_numberArrayMode(&s_numberArrayModeTypes[0]),
    m_data(cmw::data::DataFactory::createData()),
    m_createInt64Js(napi_create_bigint_int64)
{}

Data::~Data()
{
    // use Data::Delete
    assert(m_wrapper == nullptr);
    assert(m_jsData == nullptr);
}

static void DestructorNoGC(node_api_nogc_env env, void *data, void *hint)
{
#if defined(NAPI_EXPERIMENTAL)
    node_api_post_finalizer(env, Data::Destructor, data, hint);
#else
    Data::Destructor(env, data, hint);
#endif
}

void Data::Destructor(napi_env env,
                      void *nativeObject,
                      void * /* finalize_hint */)
{
    reinterpret_cast<Data *>(nativeObject)->Delete(env);
}

napi_ref Data::s_constructor = nullptr;
constexpr Data::NumberTypeValues Data::s_numberTypes[];
constexpr Data::BigIntModeValue Data::s_bigIntOutputTypes[];
constexpr Data::NumberArrayModeValue Data::s_numberArrayModeTypes[];

napi_value Data::Init(napi_env env, napi_value /* exports */)
{
    napi_value type;
    NAPI_CALL(napi_create_object(env, &type));

    for (uint32_t i = 0; i < sizeof(s_numberTypes) / sizeof(NumberTypeValues);
         ++i)
    {
        napi_value v;
        NAPI_CALL(napi_create_uint32(
            env, static_cast<uint32_t>(s_numberTypes[i].type), &v));
        NAPI_CALL(napi_set_named_property(env, type, s_numberTypes[i].name, v));
    }

    napi_value bigIntOutputType;
    NAPI_CALL(napi_create_object(env, &bigIntOutputType));
    for (uint32_t i = 0;
         i < sizeof(s_bigIntOutputTypes) / sizeof(BigIntModeValue); ++i)
    {
        napi_value v;
        NAPI_CALL(napi_create_uint32(
            env, static_cast<uint32_t>(s_bigIntOutputTypes[i].type), &v));
        NAPI_CALL(napi_set_named_property(env, bigIntOutputType,
                                          s_bigIntOutputTypes[i].name, v));
    }

    napi_value numberArrayOutputType;
    NAPI_CALL(napi_create_object(env, &numberArrayOutputType));
    for (uint32_t i = 0;
         i < sizeof(s_numberArrayModeTypes) / sizeof(NumberArrayModeValue); ++i)
    {
        napi_value v;
        NAPI_CALL(napi_create_uint32(
            env, static_cast<uint32_t>(s_numberArrayModeTypes[i].type), &v));
        NAPI_CALL(napi_set_named_property(env, numberArrayOutputType,
                                          s_numberArrayModeTypes[i].name, v));
    }

    napi_property_descriptor properties[] = {
        DECLARE_NAPI_METHOD("append", Data::Append),
        DECLARE_NAPI_METHOD("remove", Data::DeleteEntry),
        DECLARE_NAPI_METHOD("clear", Data::ClearEntries),
        DECLARE_NAPI_METHOD("getDataType", Data::GetDataType),
        DECLARE_NAPI_METHOD("setBigIntMode", Data::SetBigIntMode),
        DECLARE_NAPI_METHOD("getBigIntMode", Data::GetBigIntMode),
        DECLARE_NAPI_METHOD("setNumberArrayMode", Data::SetNumberArrayMode),
        DECLARE_NAPI_METHOD("getNumberArrayMode", Data::GetNumberArrayMode),
        DECLARE_NAPI_VALUE_ATTR("NumberType", type,
                                static_cast<napi_property_attributes>(
                                    napi_static | napi_enumerable)),
        DECLARE_NAPI_VALUE_ATTR("BigIntOutputType", bigIntOutputType,
                                static_cast<napi_property_attributes>(
                                    napi_static | napi_enumerable)),
        DECLARE_NAPI_VALUE_ATTR("NumberArrayOutputType", numberArrayOutputType,
                                static_cast<napi_property_attributes>(
                                    napi_static | napi_enumerable))};

    napi_value cons;
    NAPI_CALL(
        napi_define_class(env, "Data", NAPI_AUTO_LENGTH, New, nullptr,
                          sizeof(properties) / sizeof(napi_property_descriptor),
                          properties, &cons));

    NAPI_CALL(napi_create_reference(env, cons, 1, &s_constructor));
    return cons;
}

napi_value Data::New(napi_env env, napi_callback_info info)
{
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, nullptr, nullptr, &jsthis, nullptr));

    Data *obj = new Data();
    NAPI_CALL(napi_wrap(env, jsthis, reinterpret_cast<void *>(obj),
                        DestructorNoGC, nullptr, /* finalize_hint */
                        &obj->m_wrapper));

    // Define properties for this object
    napi_property_descriptor properties[] = {
        DECLARE_NAPI_GETTER_ATTR("entries", Data::Entries, napi_enumerable)};
    NAPI_CALL(napi_define_properties(env, jsthis, 1, properties));

    return jsthis;
}

void Data::Delete(napi_env env)
{
    napi_delete_reference(env, m_wrapper);
    m_wrapper = nullptr;
    napi_delete_reference(env, m_jsData);
    m_jsData = nullptr;

    delete this;
}

napi_value Data::NewInstance(napi_env env, const cmw::data::Data &data)
{
    napi_value instance;

    napi_value cons;
    cons = GetConstructor(env);
    NAPI_CALL(napi_new_instance(env, cons, 0, nullptr, &instance));

    Data *obj;
    NAPI_CALL(napi_unwrap(env, instance, reinterpret_cast<void **>(&obj)));

    // Sets Data object
    obj->m_data = data.clone();

    return instance;
}

napi_status Data::NewInstance(napi_env env, napi_value *instance)
{
    napi_status status;

    napi_value cons;
    cons = GetConstructor(env);
    status = napi_get_reference_value(env, s_constructor, &cons);
    if (status != napi_ok)
        return status;

    status = napi_new_instance(env, cons, 0, nullptr, instance);
    return status;
}

napi_value Data::GetConstructor(napi_env env)
{
    napi_value ret = NULL;
    NAPI_CALL(napi_get_reference_value(env, s_constructor, &ret));
    return ret;
}

std::unique_ptr<cmw::data::Data> Data::cloneData() const
{
    return m_data->clone();
}

napi_value Data::Append(napi_env env, napi_callback_info info)
{
    size_t argc = 3;
    napi_value args[3];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, args, &jsthis, nullptr));

    if (argc < 2)
    {
        napi_throw_error(env, "EINVAL", "Need tag name and value");
        return jsthis;
    }

    std::string name;
    if (NAPIUtils::getUTF8String(env, name, args[0]) != napi_ok)
    {
        napi_throw_error(env, "EINVAL", "Missing tag name (string)");
        return jsthis;
    }

    // User may set the value type
    NumberType numberType(Int32);
    napi_valuetype numTypeType;
    NAPI_CALL(napi_typeof(env, args[2], &numTypeType));
    if (numTypeType == napi_number)
    {
        int typeInt;
        NAPI_CALL(napi_get_value_int32(env, args[2], &typeInt));
        const NumberTypeValues *lastNumType = &s_numberTypes[(
            sizeof(s_numberTypes) / sizeof(NumberTypeValues) - 1)];
        if (typeInt > lastNumType->type)
        {
            napi_throw_error(env, "EINVAL", "Invalid number type");
            return jsthis;
        }
        numberType = static_cast<NumberType>(typeInt);
    }
    else if (numTypeType == napi_string)
    {
        std::string typeStr;
        NAPI_CALL(NAPIUtils::getUTF8String(env, typeStr, args[2]));
        for (uint32_t i = 0;
             i < sizeof(s_numberTypes) / sizeof(NumberTypeValues); ++i)
        {
            if (NAPIUtils::caseInsensCompare(s_numberTypes[i].name, typeStr))
            {
                numberType = s_numberTypes[i].type;
                break;
            }
        }
    }
    else if (numTypeType != napi_undefined)
    {
        napi_throw_error(env, "EINVAL", "Invalid number type argument");
        return jsthis;
    }

    Data *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    // Rebuild data at next Entries call
    if (obj->m_jsData)
    {
        NAPI_CALL(napi_delete_reference(env, obj->m_jsData));
        obj->m_jsData = nullptr;
    }

    // Tests if data already contains the tag
    if (obj->m_data->exists(name))
    {
        napi_throw_error(env, "EINVAL", "Tag already exists");
        return jsthis;
    }

    // Get type of value passed
    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, args[1], &type));

    switch (type)
    {
    case napi_boolean:
    {
        bool bVal;
        NAPI_CALL(napi_get_value_bool(env, args[1], &bVal));
        obj->m_data->append(name, bVal);
    }
    break;
    case napi_number:
    {
        switch (numberType)
        {
        case Double:
        case Float:
            double dblVal;
            NAPI_CALL(napi_get_value_double(env, args[1], &dblVal));
            if (numberType == Double)
            {
                obj->m_data->append(name, dblVal);
            }
            else
            {
                obj->m_data->append(name, static_cast<float>(dblVal));
            }
            break;
        case Int64:
            int64_t lVal;
            NAPI_CALL(napi_get_value_int64(env, args[1], &lVal));
            obj->m_data->append(name, lVal);
            break;
        case Int32:
        case Int16:
        case Int8:
            int iVal;
            NAPI_CALL(napi_get_value_int32(env, args[1], &iVal));
            if (numberType == Int32)
                obj->m_data->append(name, iVal);
            if (numberType == Int16)
                obj->m_data->append(name, static_cast<int16_t>(iVal));
            if (numberType == Int8)
                obj->m_data->append(name, static_cast<int8_t>(iVal));
            break;
        }
    }
    break;
    case napi_bigint:
    {
        int64_t val;
        bool lossless;
        NAPI_CALL(napi_get_value_bigint_int64(env, args[1], &val, &lossless));
        obj->m_data->append(name, val);
    }
    break;
    case napi_string:
    {
        std::string strVal;
        NAPI_CALL(NAPIUtils::getUTF8String(env, strVal, args[1]));
        if (numTypeType != napi_undefined)
        {
            // String to number conversion
            try
            {
                AppendNumberFromString(obj, strVal, name, numberType);
            }
            catch (const std::exception &ex)
            {
                napi_throw_error(env, "EINVAL", "Number parse failed");
            }
        }
        else
        {
            obj->m_data->append(name, strVal.c_str());
        }
    }
    break;
    case napi_object:
    {
        // Test if entry is a TypedArray
        napi_status res;
        res = AppendTypedArray(env, args[1], name, obj);
        if (res == napi_ok)
        {
            break;
        }
        res = AppendArray(env, args[1], name, obj, numberType,
                          (numTypeType != napi_undefined));
        if (res != napi_ok)
            napi_throw_error(env, "EINVAL", "Unsupported data type");
        break;
    }
    default: napi_throw_error(env, "EINVAL", "Unsupported data type"); break;
    }

    return jsthis;
}

napi_status Data::AppendTypedArray(napi_env env,
                                   napi_value value,
                                   const std::string &name,
                                   Data *obj)
{
    napi_status res;
    // Try to get it as TypedArray
    napi_typedarray_type arrType;
    size_t arrLen, byte_offset;
    void *data;
    napi_value arrayBuffer;
    res = napi_get_typedarray_info(env, value, &arrType, &arrLen, &data,
                                   &arrayBuffer, &byte_offset);
    if (res != napi_ok)
        return res;

    switch (arrType)
    {
    case napi_int8_array:
        obj->m_data->appendArray(name, (int8_t *) data, arrLen);
        break;
    case napi_int16_array:
        obj->m_data->appendArray(name, (int16_t *) data, arrLen);
        break;
    case napi_int32_array:
        obj->m_data->appendArray(name, (int32_t *) data, arrLen);
        break;
    case napi_float32_array:
        obj->m_data->appendArray(name, (float *) data, arrLen);
        break;
    case napi_float64_array:
        obj->m_data->appendArray(name, (double *) data, arrLen);
        break;
    case napi_bigint64_array:
        obj->m_data->appendArray(name, (int64_t *) data, arrLen);
        break;
    default: return napi_invalid_arg;
    }

    return res;
}

napi_status Data::AppendArray(napi_env env,
                              napi_value value,
                              const std::string &name,
                              Data *obj,
                              NumberType type,
                              bool interpretNumber)
{
    napi_status res;
    uint32_t arrayLen;
    res = napi_get_array_length(env, value, &arrayLen);
    if (res != napi_ok)
        return res;

    // Do not append empty array (we don't know what type is)
    if (arrayLen == 0)
        return res;

    napi_value firstCell;
    NAPI_CALL(napi_get_element(env, value, 0, &firstCell));
    // Array can contain : Number, bool, string, TypedArray (2D)
    napi_valuetype jsType;
    NAPI_CALL(napi_typeof(env, firstCell, &jsType));

    switch (jsType)
    {
    case napi_boolean:
        res = ApppendArray<bool>(
            env, obj, name, value, jsType,
            [](napi_env env, bool *ptr, napi_value value) {
                return napi_get_value_bool(env, value, ptr);
            },
            arrayLen);
        break;
    case napi_number:
    {
        switch (type)
        {
        case Double:
            res = ApppendArray<double>(
                env, obj, name, value, jsType,
                [](napi_env env, double *ptr, napi_value value) {
                    return napi_get_value_double(env, value, ptr);
                },
                arrayLen);
            break;
        case Float:
            res = ApppendArray<float>(
                env, obj, name, value, jsType,
                [](napi_env env, float *ptr, napi_value value) {
                    double dblVal;
                    napi_status res = napi_get_value_double(env, value, &dblVal);
                    *ptr = static_cast<float>(dblVal);
                    return res;
                },
                arrayLen);
            break;
        case Int64:
            res = ApppendArray<int64_t>(
                env, obj, name, value, jsType,
                [](napi_env env, int64_t *ptr, napi_value value) {
                    return napi_get_value_int64(env, value, ptr);
                },
                arrayLen);
            break;
        case Int32:
            res = ApppendArray<int32_t>(
                env, obj, name, value, jsType,
                [](napi_env env, int32_t *ptr, napi_value value) {
                    return napi_get_value_int32(env, value, ptr);
                },
                arrayLen);
            break;
        case Int16:
            res = ApppendArray<int16_t>(
                env, obj, name, value, jsType,
                [](napi_env env, int16_t *ptr, napi_value value) {
                    int32_t intVal;
                    napi_status res = napi_get_value_int32(env, value, &intVal);
                    *ptr = static_cast<int16_t>(intVal);
                    return res;
                },
                arrayLen);
            break;
        case Int8:
            res = ApppendArray<int8_t>(
                env, obj, name, value, jsType,
                [](napi_env env, int8_t *ptr, napi_value value) {
                    int32_t intVal;
                    napi_status res = napi_get_value_int32(env, value, &intVal);
                    *ptr = static_cast<int8_t>(intVal);
                    return res;
                },
                arrayLen);
            break;
        }
    }
    break;
    case napi_bigint:
        res = ApppendArray<int64_t>(
            env, obj, name, value, jsType,
            [](napi_env env, int64_t *ptr, napi_value value) {
                bool lossless;
                return napi_get_value_bigint_int64(env, value, ptr, &lossless);
            },
            arrayLen);
        break;
    case napi_string:
        if (interpretNumber)
        {
            try
            {
                switch (type)
                {
                case Double:
                    res = ApppendArray<double>(
                        env, obj, name, value, jsType,
                        [](napi_env env, double *ptr, napi_value value) {
                            std::string str;
                            napi_status res = NAPIUtils::getUTF8String(env, str,
                                                                       value);
                            if (res != napi_ok)
                                return res;
                            *ptr = std::stod(str);
                            return res;
                        },
                        arrayLen);
                    break;
                case Float:
                    res = ApppendArray<float>(
                        env, obj, name, value, jsType,
                        [](napi_env env, float *ptr, napi_value value) {
                            std::string str;
                            napi_status res = NAPIUtils::getUTF8String(env, str,
                                                                       value);
                            if (res != napi_ok)
                                return res;
                            *ptr = std::stof(str);
                            return res;
                        },
                        arrayLen);
                    break;
                case Int64:
                    res = ApppendArray<int64_t>(
                        env, obj, name, value, jsType,
                        [](napi_env env, int64_t *ptr, napi_value value) {
                            std::string str;
                            napi_status res = NAPIUtils::getUTF8String(env, str,
                                                                       value);
                            if (res != napi_ok)
                                return res;
                            *ptr = static_cast<int64_t>(std::stoll(str));
                            return res;
                        },
                        arrayLen);
                    break;
                case Int32:
                    res = ApppendArray<int32_t>(
                        env, obj, name, value, jsType,
                        [](napi_env env, int32_t *ptr, napi_value value) {
                            std::string str;
                            napi_status res = NAPIUtils::getUTF8String(env, str,
                                                                       value);
                            if (res != napi_ok)
                                return res;
                            *ptr = static_cast<int32_t>(std::stol(str));
                            return res;
                        },
                        arrayLen);
                    break;
                case Int16:
                    res = ApppendArray<int16_t>(
                        env, obj, name, value, jsType,
                        [](napi_env env, int16_t *ptr, napi_value value) {
                            std::string str;
                            napi_status res = NAPIUtils::getUTF8String(env, str,
                                                                       value);
                            if (res != napi_ok)
                                return res;
                            *ptr = static_cast<int16_t>(std::stol(str));
                            return res;
                        },
                        arrayLen);
                    break;
                case Int8:
                    res = ApppendArray<int8_t>(
                        env, obj, name, value, jsType,
                        [](napi_env env, int8_t *ptr, napi_value value) {
                            std::string str;
                            napi_status res = NAPIUtils::getUTF8String(env, str,
                                                                       value);
                            if (res != napi_ok)
                                return res;
                            *ptr = static_cast<int8_t>(std::stol(str));
                            return res;
                        },
                        arrayLen);
                    break;
                }
            }
            catch (const std::exception &ex)
            {
                napi_throw_error(env, "EINVAL", "Number parse failed");
            }
        }
        else
        {
            std::vector<std::string> strings;
            strings.reserve(arrayLen);
            res = ApppendArray<const char *>(
                env, obj, name, value, jsType,
                [&strings](napi_env env, const char **ptr, napi_value value) {
                    std::string str;
                    napi_status res = NAPIUtils::getUTF8String(env, str, value);
                    if (res != napi_ok)
                        return res;
                    strings.push_back(str);
                    *ptr = strings.back().data();
                    return res;
                },
                arrayLen);
        }
        break;
    case napi_object:
        if (AppendTyped2DArray(env, value, name, obj) == napi_ok)
            break;
        else if (Append2DArray(env, value, name, obj, type, interpretNumber) ==
                 napi_ok)
            break;
        else if (AppendFunction(env, value, name, obj) == napi_ok)
            break;
        else
            res = napi_invalid_arg;
        break;
    default: res = napi_invalid_arg; break;
    }
    return res;
}

napi_status Data::AppendFunction(napi_env env,
                                 napi_value value,
                                 const std::string &name,
                                 Data *obj)
{
    napi_status res = napi_invalid_arg;

    uint32_t arrayLen;
    res = napi_get_array_length(env, value, &arrayLen);
    if (res != napi_ok)
        return res;

    // Do not append empty array (we don't know what type is)
    if (arrayLen == 0)
        return res;

    std::vector<double> xArray;
    std::vector<double> yArray;
    xArray.reserve(arrayLen);
    yArray.reserve(arrayLen);

    for (uint32_t i = 0; i < arrayLen; ++i)
    {
        napi_value cell;
        NAPI_CALL(napi_get_element(env, value, i, &cell));
        // Extract X
        napi_value xElement;
        res = napi_get_named_property(env, cell, "x", &xElement);
        if (res != napi_ok)
        {
            return res;
        }
        napi_valuetype type;
        NAPI_CALL(napi_typeof(env, xElement, &type));
        double xVal;
        if (type == napi_number)
        {
            NAPI_CALL(napi_get_value_double(env, xElement, &xVal));
        }
        else if (type == napi_string)
        {
            // String to number conversion
            try
            {
                std::string strVal;
                NAPIUtils::getUTF8String(env, strVal, xElement);
                xVal = std::stod(strVal);
            }
            catch (const std::exception &ex)
            {
                napi_throw_error(env, "EINVAL", "Number parse failed");
                return napi_invalid_arg;
            }
        }
        else
        {
            NAPI_CALL(napi_throw_type_error(env, "EINVAL", "Bad x value"));
            return napi_invalid_arg;
        }
        xArray.push_back(xVal);
        // Extract Y
        napi_value yElement;
        res = napi_get_named_property(env, cell, "y", &yElement);
        if (res != napi_ok)
        {
            return res;
        }
        double yVal;
        if (type == napi_number)
        {
            NAPI_CALL(napi_get_value_double(env, yElement, &yVal));
        }
        else if (type == napi_string)
        {
            // String to number conversion
            try
            {
                std::string strVal;
                NAPIUtils::getUTF8String(env, strVal, yElement);
                yVal = std::stod(strVal);
            }
            catch (const std::exception &ex)
            {
                napi_throw_error(env, "EINVAL", "Number parse failed");
                return napi_invalid_arg;
            }
        }
        else
        {
            NAPI_CALL(napi_throw_type_error(env, "EINVAL", "Bad y value"));
            return napi_invalid_arg;
        }
        yArray.push_back(yVal);
    }
    try
    {
        std::unique_ptr<const cmw::data::DiscreteFunction> func =
            cmw::data::DataFactory::createDiscreteFunction(
                xArray.data(), yArray.data(), arrayLen);
        obj->m_data->append(name, *func);
    }
    catch (const cmw::data::DataException &ex)
    {
        napi_throw_type_error(env, "EINVAL", ex.what());
        return napi_invalid_arg;
    }
    return res;
}

napi_status Data::AppendTyped2DArray(napi_env env,
                                     napi_value value,
                                     const std::string &name,
                                     Data *obj)
{
    napi_status res;
    uint32_t arrayLen;
    res = napi_get_array_length(env, value, &arrayLen);
    if (res != napi_ok)
        return res;

    // Do not append empty array (we don't know what type is)
    if (arrayLen == 0)
        return res;

    napi_value firstCell;
    NAPI_CALL(napi_get_element(env, value, 0, &firstCell));

    // Try to get it as TypedArray
    napi_typedarray_type arrType;
    size_t arrLen, byte_offset;
    void *data;
    napi_value arrayBuffer;
    res = napi_get_typedarray_info(env, firstCell, &arrType, &arrLen, &data,
                                   &arrayBuffer, &byte_offset);
    if (res != napi_ok)
        return res;

    switch (arrType)
    {
    case napi_int8_array:
        res = Append2DTypedArray<int8_t>(env, obj, name, arrType, value,
                                         arrayLen, arrLen);
        break;
    case napi_int16_array:
        res = Append2DTypedArray<int16_t>(env, obj, name, arrType, value,
                                          arrayLen, arrLen);
        break;
    case napi_int32_array:
        res = Append2DTypedArray<int32_t>(env, obj, name, arrType, value,
                                          arrayLen, arrLen);
        break;
    case napi_float32_array:
        res = Append2DTypedArray<float>(env, obj, name, arrType, value,
                                        arrayLen, arrLen);
        break;
    case napi_float64_array:
        res = Append2DTypedArray<double>(env, obj, name, arrType, value,
                                         arrayLen, arrLen);
        break;
    case napi_bigint64_array:
        res = Append2DTypedArray<int64_t>(env, obj, name, arrType, value,
                                          arrayLen, arrLen);
        break;
    default: return napi_invalid_arg;
    }
    return res;
}

napi_status Data::Append2DArray(napi_env env,
                                napi_value value,
                                const std::string &name,
                                Data *obj,
                                NumberType type,
                                bool interpretNumber)
{
    napi_status res;
    uint32_t arrayLen;
    res = napi_get_array_length(env, value, &arrayLen);
    if (res != napi_ok)
        return res;

    // Do not append empty array (we don't know what type is)
    if (arrayLen == 0)
        return res;

    napi_value firstCell;
    NAPI_CALL(napi_get_element(env, value, 0, &firstCell));

    uint32_t firstArrayLen;
    res = napi_get_array_length(env, firstCell, &firstArrayLen);
    if (res != napi_ok)
        return res;

    if (firstArrayLen == 0)
        return res;

    // Get type of the first element
    NAPI_CALL(napi_get_element(env, firstCell, 0, &firstCell));
    napi_valuetype firstCellType;
    NAPI_CALL(napi_typeof(env, firstCell, &firstCellType));

    switch (firstCellType)
    {
    case napi_boolean:
        res = Apppend2DArray<bool>(
            env, obj, name, value, firstCellType,
            [](napi_env env, bool *ptr, napi_value cellValue) {
                return napi_get_value_bool(env, cellValue, ptr);
            },
            arrayLen, firstArrayLen);
        break;
    case napi_number:
    {
        switch (type)
        {
        case Double:
            res = Apppend2DArray<double>(
                env, obj, name, value, firstCellType,
                [](napi_env env, double *ptr, napi_value cellValue) {
                    return napi_get_value_double(env, cellValue, ptr);
                },
                arrayLen, firstArrayLen);
            break;
        case Float:
            res = Apppend2DArray<float>(
                env, obj, name, value, firstCellType,
                [](napi_env env, float *ptr, napi_value cellValue) {
                    double tmp;
                    napi_status ret = napi_get_value_double(env, cellValue,
                                                            &tmp);
                    *ptr = static_cast<float>(tmp);
                    return ret;
                },
                arrayLen, firstArrayLen);
            break;
        case Int64:
            res = Apppend2DArray<int64_t>(
                env, obj, name, value, firstCellType,
                [](napi_env env, int64_t *ptr, napi_value cellValue) {
                    return napi_get_value_int64(env, cellValue, ptr);
                },
                arrayLen, firstArrayLen);
            break;
        case Int32:
            res = Apppend2DArray<int32_t>(
                env, obj, name, value, firstCellType,
                [](napi_env env, int32_t *ptr, napi_value cellValue) {
                    return napi_get_value_int32(env, cellValue, ptr);
                },
                arrayLen, firstArrayLen);
            break;
        case Int16:
            res = Apppend2DArray<int16_t>(
                env, obj, name, value, firstCellType,
                [](napi_env env, int16_t *ptr, napi_value cellValue) {
                    int32_t tmp;
                    napi_status ret = napi_get_value_int32(env, cellValue, &tmp);
                    *ptr = static_cast<int16_t>(tmp);
                    return ret;
                },
                arrayLen, firstArrayLen);
            break;
        case Int8:
            res = Apppend2DArray<int8_t>(
                env, obj, name, value, firstCellType,
                [](napi_env env, int8_t *ptr, napi_value cellValue) {
                    int32_t tmp;
                    napi_status ret = napi_get_value_int32(env, cellValue, &tmp);
                    *ptr = static_cast<int8_t>(tmp);
                    return ret;
                },
                arrayLen, firstArrayLen);
            break;
        }
    }
    break;
    case napi_bigint:
        res = Apppend2DArray<int64_t>(
            env, obj, name, value, firstCellType,
            [](napi_env env, int64_t *ptr, napi_value cellValue) {
                bool lossless;
                napi_status ret = napi_get_value_bigint_int64(env, cellValue,
                                                              ptr, &lossless);
                return ret;
            },
            arrayLen, firstArrayLen);
        break;
    case napi_string:
        if (interpretNumber)
        {
            try
            {
                switch (type)
                {
                case Double:
                    res = Apppend2DArray<double>(
                        env, obj, name, value, firstCellType,
                        [](napi_env env, double *ptr, napi_value cellValue) {
                            std::string str;
                            napi_status res = NAPIUtils::getUTF8String(
                                env, str, cellValue);
                            if (res != napi_ok)
                                return res;
                            *ptr = std::stod(str);
                            return res;
                        },
                        arrayLen, firstArrayLen);
                    break;
                case Float:
                    res = Apppend2DArray<float>(
                        env, obj, name, value, firstCellType,
                        [](napi_env env, float *ptr, napi_value cellValue) {
                            std::string str;
                            napi_status res = NAPIUtils::getUTF8String(
                                env, str, cellValue);
                            if (res != napi_ok)
                                return res;
                            *ptr = std::stof(str);
                            return res;
                        },
                        arrayLen, firstArrayLen);
                    break;
                case Int64:
                    res = Apppend2DArray<int64_t>(
                        env, obj, name, value, firstCellType,
                        [](napi_env env, int64_t *ptr, napi_value cellValue) {
                            std::string str;
                            napi_status res = NAPIUtils::getUTF8String(
                                env, str, cellValue);
                            if (res != napi_ok)
                                return res;
                            *ptr = static_cast<int64_t>(std::stoll(str));
                            return res;
                        },
                        arrayLen, firstArrayLen);
                    break;
                case Int32:
                    res = Apppend2DArray<int32_t>(
                        env, obj, name, value, firstCellType,
                        [](napi_env env, int32_t *ptr, napi_value cellValue) {
                            std::string str;
                            napi_status res = NAPIUtils::getUTF8String(
                                env, str, cellValue);
                            if (res != napi_ok)
                                return res;
                            *ptr = static_cast<int32_t>(std::stol(str));
                            return res;
                        },
                        arrayLen, firstArrayLen);
                    break;
                case Int16:
                    res = Apppend2DArray<int16_t>(
                        env, obj, name, value, firstCellType,
                        [](napi_env env, int16_t *ptr, napi_value cellValue) {
                            std::string str;
                            napi_status res = NAPIUtils::getUTF8String(
                                env, str, cellValue);
                            if (res != napi_ok)
                                return res;
                            *ptr = static_cast<int16_t>(std::stol(str));
                            return res;
                        },
                        arrayLen, firstArrayLen);
                    break;
                case Int8:
                    res = Apppend2DArray<int8_t>(
                        env, obj, name, value, firstCellType,
                        [](napi_env env, int8_t *ptr, napi_value cellValue) {
                            std::string str;
                            napi_status res = NAPIUtils::getUTF8String(
                                env, str, cellValue);
                            if (res != napi_ok)
                                return res;
                            *ptr = static_cast<int8_t>(std::stol(str));
                            return res;
                        },
                        arrayLen, firstArrayLen);
                    break;
                }
            }
            catch (const std::exception &ex)
            {
                napi_throw_error(env, "EINVAL", "Number parse failed");
            }
        }
        else
        {
            std::vector<std::string> strings;
            strings.reserve(arrayLen * firstArrayLen);
            res = Apppend2DArray<const char *>(
                env, obj, name, value, firstCellType,
                [&strings](napi_env env, const char **ptr,
                           napi_value cellValue) {
                    std::string str;
                    napi_status res = NAPIUtils::getUTF8String(env, str,
                                                               cellValue);
                    if (res != napi_ok)
                        return res;
                    strings.push_back(str);
                    *ptr = strings.back().data();
                    return res;
                },
                arrayLen, firstArrayLen);
        }
        break;
    default: res = napi_invalid_arg; break;
    }
    return res;
}

napi_value Data::Entries(napi_env env, napi_callback_info info)
{
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, nullptr, nullptr, &jsthis, nullptr));

    Data *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    napi_value ret;
    if (obj->m_jsData)
    {
        NAPI_CALL(napi_get_reference_value(env, obj->m_jsData, &ret));
        if (ret != nullptr)
            return ret;
    }

    if (obj->m_data.get() != nullptr)
    {
        NAPI_CALL(napi_create_object(env, &ret));
        size_t arrSize = 0;
        std::vector<const cmw::data::Entry *> entries = obj->m_data->getEntries();
        for (auto const &entry : entries)
        {
            napi_value napiData;
            switch (entry->getType())
            {
            case cmw::data::DT_BOOL:
                NAPI_CALL(napi_get_boolean(env, entry->getBool(), &napiData));
                break;
            case cmw::data::DT_BYTE:
                NAPI_CALL(CreateJSNumber(env, entry->getByte(), &napiData));
                break;
            case cmw::data::DT_SHORT:
                NAPI_CALL(CreateJSNumber(env, entry->getShort(), &napiData));
                break;
            case cmw::data::DT_INT:
                NAPI_CALL(CreateJSNumber(env, entry->getInt(), &napiData));
                break;
            case cmw::data::DT_FLOAT:
                NAPI_CALL(CreateJSNumber(env, entry->getFloat(), &napiData));
                break;
            case cmw::data::DT_DOUBLE:
                NAPI_CALL(CreateJSNumber(env, entry->getDouble(), &napiData));
                break;
            case cmw::data::DT_LONG:
                NAPI_CALL(
                    obj->m_createInt64Js(env, entry->getLong(), &napiData));
                break;
            case cmw::data::DT_STRING:
            {
                std::string str = entry->getString();
                NAPI_CALL(napi_create_string_utf8(env, str.c_str(),
                                                  str.length(), &napiData));
                break;
            }
            case cmw::data::DT_BOOL_ARRAY:
                NAPI_CALL(CreateJSArray(env, obj, entry->getArrayBool(arrSize),
                                        arrSize, napi_int8_array, &napiData));
                break;
            case cmw::data::DT_BYTE_ARRAY:
                NAPI_CALL(CreateJSArray(env, obj, entry->getArrayByte(arrSize),
                                        arrSize, napi_int8_array, &napiData));
                break;
            case cmw::data::DT_SHORT_ARRAY:
                NAPI_CALL(CreateJSArray(env, obj, entry->getArrayShort(arrSize),
                                        arrSize, napi_int16_array, &napiData));
                break;
            case cmw::data::DT_INT_ARRAY:
                NAPI_CALL(CreateJSArray(env, obj, entry->getArrayInt(arrSize),
                                        arrSize, napi_int32_array, &napiData));
                break;
            case cmw::data::DT_FLOAT_ARRAY:
                NAPI_CALL(CreateJSArray(env, obj, entry->getArrayFloat(arrSize),
                                        arrSize, napi_float32_array, &napiData));
                break;
            case cmw::data::DT_DOUBLE_ARRAY:
                NAPI_CALL(CreateJSArray(env, obj,
                                        entry->getArrayDouble(arrSize), arrSize,
                                        napi_float64_array, &napiData));
                break;
            case cmw::data::DT_LONG_ARRAY:
                NAPI_CALL(CreateJSArray(env, obj, entry->getArrayLong(arrSize),
                                        arrSize, napi_bigint64_array,
                                        &napiData));
                break;
            case cmw::data::DT_STRING_ARRAY:
                NAPI_CALL(CreateJSArray(env, obj, entry->getArrayString(arrSize),
                                        arrSize, &napiData));
                break;
            case cmw::data::DT_BOOL_ARRAY_2D:
                size_t colCount;
                NAPI_CALL(CreateJS2DArray(
                    env, obj, entry->getArrayBool2D(arrSize, colCount), arrSize,
                    colCount, napi_int8_array, &napiData));
                break;
            case cmw::data::DT_BYTE_ARRAY_2D:
                NAPI_CALL(CreateJS2DArray(
                    env, obj, entry->getArrayByte2D(arrSize, colCount), arrSize,
                    colCount, napi_int8_array, &napiData));
                break;
            case cmw::data::DT_SHORT_ARRAY_2D:
                NAPI_CALL(CreateJS2DArray(
                    env, obj, entry->getArrayShort2D(arrSize, colCount),
                    arrSize, colCount, napi_int16_array, &napiData));
                break;
            case cmw::data::DT_INT_ARRAY_2D:
                NAPI_CALL(CreateJS2DArray(
                    env, obj, entry->getArrayInt2D(arrSize, colCount), arrSize,
                    colCount, napi_int32_array, &napiData));
                break;
            case cmw::data::DT_FLOAT_ARRAY_2D:
                NAPI_CALL(CreateJS2DArray(
                    env, obj, entry->getArrayFloat2D(arrSize, colCount),
                    arrSize, colCount, napi_float32_array, &napiData));
                break;
            case cmw::data::DT_DOUBLE_ARRAY_2D:
                NAPI_CALL(CreateJS2DArray(
                    env, obj, entry->getArrayDouble2D(arrSize, colCount),
                    arrSize, colCount, napi_float64_array, &napiData));
                break;
            case cmw::data::DT_LONG_ARRAY_2D:
                NAPI_CALL(CreateJS2DArray(
                    env, obj, entry->getArrayLong2D(arrSize, colCount), arrSize,
                    colCount, napi_bigint64_array, &napiData));
                break;
            case cmw::data::DT_STRING_ARRAY_2D:
                NAPI_CALL(CreateJS2DArray(
                    env, obj, entry->getArrayString2D(arrSize, colCount),
                    arrSize, colCount, &napiData));
                break;
            case cmw::data::DT_DISCRETE_FUNCTION:
                NAPI_CALL(CreateJSFunction(
                    env, obj, entry->getDiscreteFunction(), &napiData));
                break;
            default: NAPI_CALL(napi_get_undefined(env, &napiData));
            }
            // Set property name
            NAPI_CALL(
                napi_set_named_property(env, ret, entry->getName(), napiData));
        }
        // Save a reference to the object
        NAPI_CALL(napi_create_reference(env, ret, 1, &obj->m_jsData));
    }
    else
    {
        NAPI_CALL(napi_get_undefined(env, &ret));
    }
    return ret;
}

napi_status Data::CreateJSFunction(napi_env env,
                                   Data * /*obj*/,
                                   const cmw::data::DiscreteFunction &function,
                                   napi_value *result)
{
    size_t funcLen = function.getSize();
    const double *xArray = function.getXArray();
    const double *yArray = function.getYArray();

    NAPI_CALL(napi_create_array_with_length(env, function.getSize(), result));
    for (size_t i = 0; i < funcLen; ++i)
    {
        napi_value cell;
        napi_value xValue;
        napi_value yValue;

        NAPI_CALL(napi_create_object(env, &cell));

        NAPI_CALL(napi_create_double(env, xArray[i], &xValue));
        NAPI_CALL(napi_create_double(env, yArray[i], &yValue));
        NAPI_CALL(napi_set_named_property(env, cell, "x", xValue));
        NAPI_CALL(napi_set_named_property(env, cell, "y", yValue));
        NAPI_CALL(napi_set_element(env, *result, i, cell));
    }
    return napi_ok;
}

napi_value Data::GetDataType(napi_env env, napi_callback_info info)
{
    napi_value ret;

    size_t argc = 1;
    napi_value args[1];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, args, &jsthis, nullptr));

    std::string name;
    if (NAPIUtils::getUTF8String(env, name, args[0]) != napi_ok)
    {
        napi_throw_error(env, "EINVAL", "Missing tag name (string)");
        return jsthis;
    }

    Data *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    // Gets the entry
    const cmw::data::Entry *entry = obj->m_data->getEntry(name);
    if (!entry)
    {
        NAPI_CALL(napi_get_undefined(env, &ret));
    }
    else
    {
        std::string value = "undefined";
        try
        {
            value = cmw::data::DataTypeUtils::toString(entry->getType());
        }
        catch (...)
        {}
        NAPI_CALL(napi_create_string_utf8(env, value.c_str(), NAPI_AUTO_LENGTH,
                                          &ret));
    }
    return ret;
}

napi_value Data::SetBigIntMode(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value args[1];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, args, &jsthis, nullptr));
    Data *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype argType;
    NAPI_CALL(napi_typeof(env, args[0], &argType));
    const Data::BigIntModeValue *newType = nullptr;
    if (argType == napi_string)
    {
        std::string typeStr;
        NAPI_CALL(NAPIUtils::getUTF8String(env, typeStr, args[0]));
        newType = getBigIntModeValue(typeStr);
    }
    else if (argType == napi_number)
    {
        int32_t typeInt;
        NAPI_CALL(napi_get_value_int32(env, args[0], &typeInt));
        newType = getBigIntModeValue(typeInt);
    }
    if (newType == nullptr)
    {
        napi_throw_error(env, "EINVAL", "Invalid argument");
        return jsthis;
    }

    if (obj->m_int64Mode->type != newType->type)
    {
        obj->m_int64Mode = newType;
        switch (obj->m_int64Mode->type)
        {
        case String: obj->m_createInt64Js = NAPIUtils::int64ToJSString; break;
        case Timestamp:
            obj->m_createInt64Js = NAPIUtils::int64ToTimeStamp;
            break;
        default: obj->m_createInt64Js = napi_create_bigint_int64; break;
        }
        // Clear cache
        if (obj->m_jsData)
        {
            NAPI_CALL(napi_delete_reference(env, obj->m_jsData));
            obj->m_jsData = nullptr;
        }
    }
    return jsthis;
}

napi_value Data::GetBigIntMode(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value args[1];
    napi_value jsthis;
    napi_value ret;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, args, &jsthis, nullptr));
    Data *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype argType;
    NAPI_CALL(napi_typeof(env, args[0], &argType));
    if (argType == napi_string)
    {
        NAPI_CALL(napi_create_string_utf8(env, obj->m_int64Mode->name,
                                          NAPI_AUTO_LENGTH, &ret));
    }
    else
    {
        NAPI_CALL(napi_create_int32(env, obj->m_int64Mode->type, &ret));
    }
    return ret;
}

const Data::BigIntModeValue *Data::getBigIntModeValue(int32_t type)
{
    BigIntModeType newType = static_cast<BigIntModeType>(type);
    for (uint32_t i = 0;
         i < sizeof(s_bigIntOutputTypes) / sizeof(BigIntModeValue); ++i)
    {
        if (s_bigIntOutputTypes[i].type == newType)
        {
            return &s_bigIntOutputTypes[i];
        }
    }
    return &s_bigIntOutputTypes[0];
}

const Data::BigIntModeValue *Data::getBigIntModeValue(const std::string &type)
{
    for (uint32_t i = 0;
         i < sizeof(s_bigIntOutputTypes) / sizeof(BigIntModeValue); ++i)
    {
        if (NAPIUtils::caseInsensCompare(s_bigIntOutputTypes[i].name, type))
        {
            return &s_bigIntOutputTypes[i];
        }
    }
    return &s_bigIntOutputTypes[0];
}

napi_value Data::SetNumberArrayMode(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value args[1];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, args, &jsthis, nullptr));
    Data *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype argType;
    NAPI_CALL(napi_typeof(env, args[0], &argType));
    const Data::NumberArrayModeValue *newType = nullptr;
    if (argType == napi_string)
    {
        std::string typeStr;
        NAPI_CALL(NAPIUtils::getUTF8String(env, typeStr, args[0]));
        newType = getNumberArrayModeValue(typeStr);
    }
    else if (argType == napi_number)
    {
        int32_t typeInt;
        NAPI_CALL(napi_get_value_int32(env, args[0], &typeInt));
        newType = getNumberArrayModeValue(typeInt);
    }
    if (newType == nullptr)
    {
        napi_throw_error(env, "EINVAL", "Invalid argument");
        return jsthis;
    }

    if (obj->m_numberArrayMode->type != newType->type)
    {
        obj->m_numberArrayMode = newType;
        // Clear cache
        if (obj->m_jsData)
        {
            NAPI_CALL(napi_delete_reference(env, obj->m_jsData));
            obj->m_jsData = nullptr;
        }
    }
    return jsthis;
}

napi_value Data::GetNumberArrayMode(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value args[1];
    napi_value jsthis;
    napi_value ret;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, args, &jsthis, nullptr));
    Data *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype argType;
    NAPI_CALL(napi_typeof(env, args[0], &argType));
    if (argType == napi_string)
    {
        NAPI_CALL(napi_create_string_utf8(env, obj->m_numberArrayMode->name,
                                          NAPI_AUTO_LENGTH, &ret));
    }
    else
    {
        NAPI_CALL(napi_create_int32(env, obj->m_numberArrayMode->type, &ret));
    }
    return ret;
}

napi_value Data::DeleteEntry(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value args[1];
    napi_value jsthis;
    napi_value ret;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, args, &jsthis, nullptr));
    Data *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype argType;
    NAPI_CALL(napi_typeof(env, args[0], &argType));
    if (argType == napi_string)
    {
        std::string tagName;
        NAPI_CALL(NAPIUtils::getUTF8String(env, tagName, args[0]));
        obj->m_data->remove(tagName);
        // Clear cache
        if (obj->m_jsData)
        {
            NAPI_CALL(napi_delete_reference(env, obj->m_jsData));
            obj->m_jsData = nullptr;
        }
    }
    napi_get_undefined(env, &ret);
    return ret;
}

napi_value Data::ClearEntries(napi_env env, napi_callback_info info)
{
    size_t argc = 0;
    napi_value jsthis;
    napi_value ret;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, nullptr, &jsthis, nullptr));
    Data *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    obj->m_data->clear();
    // Clear cache
    if (obj->m_jsData)
    {
        NAPI_CALL(napi_delete_reference(env, obj->m_jsData));
        obj->m_jsData = nullptr;
    }
    napi_get_undefined(env, &ret);
    return ret;
}

const Data::NumberArrayModeValue *Data::getNumberArrayModeValue(int32_t type)
{
    NumberArrayModeType newType = static_cast<NumberArrayModeType>(type);
    for (uint32_t i = 0;
         i < sizeof(s_numberArrayModeTypes) / sizeof(NumberArrayModeValue); ++i)
    {
        if (s_numberArrayModeTypes[i].type == newType)
        {
            return &s_numberArrayModeTypes[i];
        }
    }
    return &s_numberArrayModeTypes[0];
}

const Data::NumberArrayModeValue *Data::getNumberArrayModeValue(
    const std::string &type)
{
    for (uint32_t i = 0;
         i < sizeof(s_numberArrayModeTypes) / sizeof(NumberArrayModeValue); ++i)
    {
        if (NAPIUtils::caseInsensCompare(s_numberArrayModeTypes[i].name, type))
        {
            return &s_numberArrayModeTypes[i];
        }
    }
    return &s_numberArrayModeTypes[0];
}
