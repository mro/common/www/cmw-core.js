/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#ifndef ACQUIRED_DATA_H_
#define ACQUIRED_DATA_H_

#include <cmw-rda3/common/data/AcquiredData.h>
#include <node_api.h>

class Data;

class AcquiredData
{
public:
    static napi_value Init(napi_env env);
    static void Destructor(napi_env env,
                           void *nativeObject,
                           void *finalize_hint);

    static napi_value NewInstance(
        napi_env env,
        const std::unique_ptr<cmw::rda3::common::AcquiredData> &data);
    static napi_value GetConstructor(napi_env env);
    static napi_value GetCycleName(napi_env env, napi_callback_info info);
    static napi_value SetCycleName(napi_env env, napi_callback_info info);
    static napi_value GetCycleStamp(napi_env env, napi_callback_info info);
    static napi_value SetCycleStamp(napi_env env, napi_callback_info info);
    static napi_value GetAcqStamp(napi_env env, napi_callback_info info);
    static napi_value SetAcqStamp(napi_env env, napi_callback_info info);
    static napi_value GetData(napi_env env, napi_callback_info info);
    static napi_value SetData(napi_env env, napi_callback_info info);

    const std::string &getCycleName() const;
    int64_t getCycleStamp() const;
    int64_t getAcqStamp() const;
    const Data *getData(napi_env env) const;
    std::unique_ptr<cmw::rda3::common::AcquiredContext> getContext() const;

private:
    AcquiredData();
    ~AcquiredData();
    void Delete(napi_env env);

protected:
    static napi_ref s_constructor;
    static napi_value New(napi_env env, napi_callback_info info);

    napi_ref m_wrapper;
    std::string m_cycleName;
    int64_t m_cycleStamp;
    int64_t m_acqStamp;
    napi_ref m_data;
};

class AcquiredContext : public cmw::rda3::common::AcquiredContext
{
public:
    explicit AcquiredContext(const AcquiredData *data) :
        m_cycleName(data->getCycleName()),
        m_cycleStamp(data->getCycleStamp()),
        m_acqStamp(data->getAcqStamp()),
        m_data(cmw::data::DataFactory::createData())
    {}

    const std::string &getCycleName() const override { return m_cycleName; }

    int64_t getCycleStamp() const override { return m_cycleStamp; }

    int64_t getAcqStamp() const override { return m_acqStamp; }

    const cmw::data::Data &getData() const override { return *m_data.get(); }

    std::string toString() const override { return ""; }

    bool operator==(const cmw::rda3::common::AcquiredContext &other) const override
    {
        return (this == &other) ||
            ((getCycleName() == other.getCycleName()) &&
             (getCycleStamp() == other.getCycleStamp()) &&
             (getAcqStamp() == other.getAcqStamp()) &&
             (getData() != other.getData()));
    }

    bool operator!=(const cmw::rda3::common::AcquiredContext &other) const override
    {
        return !operator==(other);
    }

private:
    std::string m_cycleName;
    int64_t m_cycleStamp;
    int64_t m_acqStamp;
    std::unique_ptr<cmw::data::Data> m_data;
};

#endif // DATA_INT32_H_
