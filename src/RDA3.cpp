/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#include <cmw-log/LogManager.h>
#include <unistd.h>

#include "AccessPoint.hpp"
#include "AcquiredData.hpp"
#include "Data.hpp"
#include "NAPIUtils.hpp"
#include "RBACConfiguration.hpp"
#include "Server.hpp"

/*napi_value*/ NAPI_MODULE_INIT(/*napi_env env, napi_value exports*/)
{
    if (access("cmw-core-log.properties", F_OK) == 0)
    {
        cmw::log::LogManager::init("cmw-core-log.properties");
    }

    napi_value newExports;
    NAPI_CALL(napi_create_object(env, &newExports));
    NAPI_CALL(napi_set_named_property(env, newExports, "AccessPoint",
                                      AccessPoint::Init(env, exports)));
    NAPI_CALL(napi_set_named_property(env, newExports, "Data",
                                      Data::Init(env, exports)));
    NAPI_CALL(napi_set_named_property(env, newExports, "RBACConfiguration",
                                      RBACConfiguration::Init(env, exports)));
    NAPI_CALL(napi_set_named_property(env, newExports, "Server",
                                      Server::Init(env, exports)));
    NAPI_CALL(napi_set_named_property(env, newExports, "AcquiredData",
                                      AcquiredData::Init(env)));

    return newExports;
}
