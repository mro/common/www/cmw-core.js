/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#include "Server.hpp"

#include <algorithm>
#include <chrono>
#include <iostream>
#include <thread>

#include <assert.h>
#include <cmw-rda3/common/exception/InvalidRequestException.h>
#include <cmw-rda3/common/exception/NameServiceException.h>

#include "AcquiredData.hpp"
#include "Data.hpp"
#include "NAPIUtils.hpp"

using namespace cmw::rda3;

Server::Server(const std::string &serverName) :
    m_serverName(serverName),
    m_getTSFunc(nullptr),
    m_getJSCallback(nullptr),
    m_setTSFunc(nullptr),
    m_setJSCallback(nullptr),
    m_subTSFunc(nullptr),
    m_subJSCallback(nullptr),
    m_unsubJSCallback(nullptr),
    m_subSrcAddedCallback(nullptr),
    m_subSrcRemovedCallback(nullptr)
{}

Server::~Server() {}

void Server::Destructor(napi_env env,
                        void *nativeObject,
                        void * /*finalize_hint*/)
{
    reinterpret_cast<Server *>(nativeObject)->Delete(env);
}

napi_ref Server::s_constructor = nullptr;

napi_value Server::Init(napi_env env, napi_value /* exports */)
{
    napi_property_descriptor properties[] = {
        DECLARE_NAPI_METHOD("start", Server::Start),
        DECLARE_NAPI_METHOD("stop", Server::Stop),
        DECLARE_NAPI_METHOD("addConfigFile", Server::AddConfigurationFile),
        DECLARE_NAPI_METHOD("setConfigProp", Server::SetConfigurationProperty),
        DECLARE_NAPI_METHOD("setGetCallback", Server::SetGetCB),
        DECLARE_NAPI_METHOD("setSetCallback", Server::SetSetCB),
        DECLARE_NAPI_METHOD("setSubscribeCallback", Server::SetSubscribeCB),
        DECLARE_NAPI_METHOD("setUnsubscribeCallback", Server::SetUnsubscribeCB),
        DECLARE_NAPI_METHOD("setSubSrcAddedCallback", Server::SetSubSrcAddedCB),
        DECLARE_NAPI_METHOD("setSubSrcRemovedCallback",
                            Server::SetSubSrcRemovedCB)};

    napi_value cons;
    NAPI_CALL(
        napi_define_class(env, "Server", NAPI_AUTO_LENGTH, New, nullptr,
                          sizeof(properties) / sizeof(napi_property_descriptor),
                          properties, &cons));

    NAPI_CALL(napi_create_reference(env, cons, 1, &s_constructor));
    return cons;
}

napi_value Server::New(napi_env env, napi_callback_info info)
{
    napi_value jsthis;
    size_t argc = 1;
    napi_value argv[1];
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    std::string serverName;
    napi_status ret;
    ret = NAPIUtils::getUTF8String(env, serverName, argv[0]);
    if (ret != napi_ok)
    {
        napi_throw_type_error(env, "EINVAL", "wrong server name");
        return jsthis;
    }

    Server *obj = new Server(serverName);
    NAPI_CALL(napi_wrap(env, jsthis, reinterpret_cast<void *>(obj),
                        Server::Destructor, nullptr, nullptr));

    return jsthis;
}

void Server::Delete(napi_env env)
{
    Stop();
    napi_delete_reference(env, m_getJSCallback);
    napi_delete_reference(env, m_setJSCallback);
    napi_delete_reference(env, m_subJSCallback);
    napi_delete_reference(env, m_unsubJSCallback);
    napi_delete_reference(env, m_subSrcAddedCallback);
    napi_delete_reference(env, m_subSrcRemovedCallback);

    m_subSources.clear();

    delete this;
}

napi_status Server::NewInstance(napi_env env, napi_value *instance)
{
    napi_status status;

    napi_value cons;
    cons = GetConstructor(env);
    status = napi_get_reference_value(env, s_constructor, &cons);
    if (status != napi_ok)
        return status;

    status = napi_new_instance(env, cons, 0, nullptr, instance);
    return status;
}

napi_value Server::GetConstructor(napi_env env)
{
    napi_value ret = NULL;
    NAPI_CALL(napi_get_reference_value(env, s_constructor, &ret));
    return ret;
}

void Server::get(std::unique_ptr<server::GetRequest> request)
{
    // Called when a client performs a get call.
    std::unique_ptr<server::GetRequest> *req =
        new std::unique_ptr<server::GetRequest>(std::move(request));
    std::lock_guard<std::mutex> lock(m_mutex);
    if (!m_getTSFunc ||
        napi_call_threadsafe_function(m_getTSFunc, req, napi_tsfn_blocking) !=
            napi_ok)
    {
        delete req;
    }
}

void Server::set(std::unique_ptr<server::SetRequest> request)
{
    // Called when a client performs a set call.
    std::unique_ptr<server::SetRequest> *req =
        new std::unique_ptr<server::SetRequest>(std::move(request));
    std::lock_guard<std::mutex> lock(m_mutex);
    if (!m_setTSFunc ||
        napi_call_threadsafe_function(m_setTSFunc, req, napi_tsfn_blocking) !=
            napi_ok)
    {
        delete req;
    }
}

void Server::subscribe(server::SubscriptionRequest &request)
{
    std::shared_ptr<ServerRequestCallback> req(
        std::make_shared<ServerRequestCallback>(
            ServerRequestCallback::SUBSCRIBE, &request,
            Server::subscribeCallback));
    std::shared_ptr<ServerRequestCallback> *reqArg =
        new std::shared_ptr<ServerRequestCallback>(req);

    std::unique_lock<std::mutex> reqLock(req->m);
    std::unique_lock<std::mutex> lock(m_mutex);
    if (!m_subTSFunc ||
        napi_call_threadsafe_function(m_subTSFunc, reqArg,
                                      napi_tsfn_blocking) != napi_ok)
    {
        lock.unlock();
        delete reqArg;
        request.reject(common::ServerException("Server being destroyed"));
    }
    else
    {
        lock.unlock();
        // Waits for JS function to be executed
        while (!req->notified && m_server)
        { // loop to avoid spurious wakeups
            req->cond_var.wait_for(reqLock, std::chrono::milliseconds(250));
        }
        req->notified = true; // warn js side that this request is dead
        if (req->out)
        {
            // No error, accept the subscription request
            server::SubscriptionCreator &creator = request.accept();

            // Send first update to this client.
            common::AcquiredData *data = static_cast<common::AcquiredData *>(
                req->out);
            creator.firstUpdate(*data);

            // This MUST be called to enable sending of immediate/normal updates
            // to this client.
            creator.startPublishing();
            delete data;
        }
        else
        {
            // Subscription rejected
            request.reject(common::ServerException(req->errMsg));
        }
    }
}

void Server::unsubscribe(const common::Request &request)
{
    std::shared_ptr<ServerRequestCallback> req(
        std::make_shared<ServerRequestCallback>(
            ServerRequestCallback::UNSUBSCRIBE, &request,
            Server::unsubscribeCallback));
    std::shared_ptr<ServerRequestCallback> *reqArg =
        new std::shared_ptr<ServerRequestCallback>(req);

    std::unique_lock<std::mutex> reqLock(req->m);
    std::unique_lock<std::mutex> lock(m_mutex);
    if (m_subTSFunc &&
        napi_call_threadsafe_function(m_subTSFunc, reqArg,
                                      napi_tsfn_blocking) == napi_ok)
    {
        lock.unlock();

        // Waits for JS function to be executed
        while (!req->notified && m_server)
        { // loop to avoid spurious wakeups
            req->cond_var.wait_for(reqLock, std::chrono::milliseconds(250));
        }
        req->notified = true; // warn js side that this request is dead
    }
    else
        delete reqArg;
}

void Server::subscriptionSourceAdded(
    const server::SubscriptionSourceSharedPtr &subscription)
{
    std::shared_ptr<ServerRequestCallback> req(
        std::make_shared<ServerRequestCallback>(
            ServerRequestCallback::NEW_SOURCE, &subscription,
            Server::subSrcChangedCallback));
    std::shared_ptr<ServerRequestCallback> *reqArg =
        new std::shared_ptr<ServerRequestCallback>(req);

    std::unique_lock<std::mutex> reqLock(req->m);
    std::unique_lock<std::mutex> lock(m_mutex);
    m_subSources[subscription->getId()] = subscription;
    if (m_subTSFunc &&
        napi_call_threadsafe_function(m_subTSFunc, reqArg,
                                      napi_tsfn_blocking) == napi_ok)
    {
        lock.unlock();
        // Waits for JS function to be executed
        while (!req->notified && m_server)
        { // loop to avoid spurious wakeups
            req->cond_var.wait_for(reqLock, std::chrono::milliseconds(250));
        }
        req->notified = true; // warn js side that this request is dead
    }
    else
        delete reqArg;
}

void Server::subscriptionSourceRemoved(
    const server::SubscriptionSourceSharedPtr &subscription)
{
    std::shared_ptr<ServerRequestCallback> req(
        std::make_shared<ServerRequestCallback>(
            ServerRequestCallback::REM_SOURCE, &subscription,
            Server::subSrcChangedCallback));
    std::shared_ptr<ServerRequestCallback> *reqArg =
        new std::shared_ptr<ServerRequestCallback>(req);

    std::unique_lock<std::mutex> reqLock(req->m);
    std::unique_lock<std::mutex> lock(m_mutex);
    // will not delete since we're called with shared-ptr
    m_subSources.erase(subscription->getId());
    if (m_subTSFunc &&
        napi_call_threadsafe_function(m_subTSFunc, reqArg,
                                      napi_tsfn_blocking) == napi_ok)
    {
        lock.unlock();
        // Waits for JS function to be executed
        while (!req->notified && m_server)
        { // loop to avoid spurious wakeups
            req->cond_var.wait_for(reqLock, std::chrono::milliseconds(250));
        }
        req->notified = true; // warn js side that this request is dead
    }
    else
        delete reqArg;
}

napi_value Server::Start(napi_env env, napi_callback_info info)
{
    napi_value jsthis;
    size_t argc = 1;
    napi_value argv[1];
    napi_value ret;
    napi_get_undefined(env, &ret);

    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    Server *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    if (obj->m_server)
    {
        napi_throw_error(env, "EEXIST", "Server already started");
        return ret;
    }

    std::unique_ptr<server::ServerBuilder> builder =
        server::ServerBuilder::newInstance();
    builder->setServerName(obj->m_serverName);
    if (obj->m_configBuilder)
        builder->setConfig(obj->m_configBuilder->build());
    builder->setRequestReplyCallback(*obj);
    builder->setSubscriptionCallback(*obj);

    try
    {
        obj->m_server = builder->build();
        obj->m_server->start(false);
        std::cerr << "ServerInfo:" << obj->m_server->getServerInfo().toString()
                  << std::endl;
    }
    catch (const std::exception &ex)
    {
        // can throw cmw::rda3::common::NameServiceException,
        // cmw::rda3::transport::TransportException
        napi_throw_error(env, "RDA3Error", ex.what());
        obj->m_server.reset();
        return ret;
    }

    std::lock_guard<std::mutex> lock(obj->m_mutex);
    if (!obj->m_getTSFunc)
    {
        napi_value work_name;
        NAPI_CALL(napi_create_string_utf8(env, "RDA3 server-get",
                                          NAPI_AUTO_LENGTH, &work_name));
        NAPI_CALL(napi_create_threadsafe_function(
            env, nullptr, nullptr, work_name, 0, 1, nullptr, nullptr, obj,
            Server::getJSReplyCallback, &(obj->m_getTSFunc)));
    }
    if (!obj->m_setTSFunc)
    {
        napi_value work_name;
        NAPI_CALL(napi_create_string_utf8(env, "RDA3 server-set",
                                          NAPI_AUTO_LENGTH, &work_name));
        NAPI_CALL(napi_create_threadsafe_function(
            env, nullptr, nullptr, work_name, 0, 1, nullptr, nullptr, obj,
            Server::setJSReplyCallback, &(obj->m_setTSFunc)));
    }
    if (!obj->m_subTSFunc)
    {
        napi_value work_name;
        NAPI_CALL(napi_create_string_utf8(env, "RDA3 server-sub",
                                          NAPI_AUTO_LENGTH, &work_name));
        NAPI_CALL(napi_create_threadsafe_function(
            env, nullptr, nullptr, work_name, 0, 1, nullptr, nullptr, obj,
            Server::subJSReplyCallback, &(obj->m_subTSFunc)));
    }
    return ret;
}

napi_value Server::Stop(napi_env env, napi_callback_info info)
{
    napi_value jsthis;

    NAPI_CALL(napi_get_cb_info(env, info, nullptr, nullptr, &jsthis, nullptr));

    Server *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    napi_value ret;
    napi_get_boolean(env, obj->Stop(), &ret);
    return ret;
}

bool Server::Stop()
{
    std::unique_ptr<cmw::rda3::server::Server> server;
    /* will set m_server as null, which will unblock waiting threads */
    m_server.swap(server);

    if (server)
    {
        server->shutdown();

        std::lock_guard<std::mutex> lock(m_mutex);
        NAPI_CALL(
            napi_release_threadsafe_function(m_getTSFunc, napi_tsfn_abort));
        m_getTSFunc = nullptr;
        NAPI_CALL(
            napi_release_threadsafe_function(m_setTSFunc, napi_tsfn_abort));
        m_setTSFunc = nullptr;
        NAPI_CALL(
            napi_release_threadsafe_function(m_subTSFunc, napi_tsfn_abort));
        m_subTSFunc = nullptr;
        return true;
    }
    return false;
}

napi_value Server::AddConfigurationFile(napi_env env, napi_callback_info info)
{
    napi_value jsthis;
    size_t argc = 1;
    napi_value argv[1];
    napi_value ret;
    napi_get_undefined(env, &ret);
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    Server *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    std::string filePath;
    napi_status res = NAPIUtils::getUTF8String(env, filePath, argv[0]);
    if (res != napi_ok)
    {
        napi_throw_type_error(env, "EINVAL", "File path (string) expected");
        return ret;
    }
    if (!obj->m_configBuilder)
    {
        obj->m_configBuilder = cmw::util::ConfigurationBuilder::newInstance();
    }
    try
    {
        obj->m_configBuilder->addFile(filePath);
    }
    catch (const std::exception &ex)
    {
        napi_throw_error(env, "ENOENT", ex.what());
    }
    return ret;
}

napi_value Server::SetConfigurationProperty(napi_env env,
                                            napi_callback_info info)
{
    napi_value jsthis;
    size_t argc = 1;
    napi_value argv[1];
    napi_value ret;
    napi_get_undefined(env, &ret);
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    napi_valuetype argType;
    NAPI_CALL(napi_typeof(env, argv[0], &argType));
    if (argType != napi_object)
    {
        napi_throw_type_error(env, "EINVAL", "Object expected");
        return ret;
    }

    Server *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    napi_value keys;
    uint32_t nbKeys = 0;
    NAPI_CALL(napi_get_property_names(env, argv[0], &keys));
    NAPI_CALL(napi_get_array_length(env, keys, &nbKeys));
    if (nbKeys > 0 && !obj->m_configBuilder)
    {
        obj->m_configBuilder = cmw::util::ConfigurationBuilder::newInstance();
    }

    for (uint32_t i = 0; i < nbKeys; ++i)
    {
        napi_value key;
        napi_value val;
        std::string keyStr;
        std::string valStr;
        NAPI_CALL(napi_get_element(env, keys, i, &key));
        NAPI_CALL(NAPIUtils::getUTF8String(env, keyStr, key));
        NAPI_CALL(napi_get_property(env, argv[0], key, &val));
        NAPI_CALL(napi_coerce_to_string(env, val, &val));
        NAPI_CALL(NAPIUtils::getUTF8String(env, valStr, val));
        obj->m_configBuilder->setProperty(keyStr, valStr);
    }
    return ret;
}

napi_value Server::SetGetCB(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value argv[1];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    Server *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, argv[0], &type));
    if (type == napi_undefined || type == napi_null || type == napi_function)
    {
        if (obj->m_getJSCallback)
            NAPI_CALL(napi_delete_reference(env, obj->m_getJSCallback));
        obj->m_getJSCallback = nullptr;
        if (type == napi_function)
        {
            NAPI_CALL(
                napi_create_reference(env, argv[0], 1, &obj->m_getJSCallback));
        }
    }
    napi_value ret;
    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

void Server::getJSReplyCallback(napi_env env,
                                napi_value /* js_callback */,
                                void *context,
                                void *vdata)
{
    Server *obj = reinterpret_cast<Server *>(context);
    std::unique_ptr<server::GetRequest> *request =
        reinterpret_cast<std::unique_ptr<server::GetRequest> *>(vdata);
    napi_status status;
    std::string err;

    if (!request)
        return;
    else if (!obj->m_getJSCallback)
        err = "No getter handler installed";
    else
    {
        napi_value argv;
        NAPI_CALL(createRequestObject(env, request->get(), &argv));

        napi_value func;
        NAPI_CALL(napi_get_reference_value(env, obj->m_getJSCallback, &func));
        napi_value cbRet;

        napi_value global;
        napi_get_global(env, &global);
        status = napi_call_function(env, global, func, 1, &argv, &cbRet);
        if (status == napi_pending_exception)
        {
            napi_value ex;
            NAPI_CALL(napi_get_and_clear_last_exception(env, &ex));
            bool isError;
            NAPI_CALL(napi_is_error(env, ex, &isError));
            if (isError)
            {
                napi_value msg;
                NAPI_CALL(napi_get_named_property(env, ex, "message", &msg));
                NAPIUtils::getUTF8String(env, err, msg);
            }
            else
                err = "unknown error";
        }
        else
        {
            bool is_instance = false;
            NAPI_CALL(napi_instanceof(env, cbRet, Data::GetConstructor(env),
                                      &is_instance));
            if (!is_instance)
                err = "Argument is not a Data object";
            else
            {
                Data *data;
                NAPI_CALL(
                    napi_unwrap(env, cbRet, reinterpret_cast<void **>(&data)));
                (*request)->requestCompleted(
                    common::AcquiredData(data->cloneData()));
            }
        }
    }

    if (!err.empty())
        (*request)->requestFailed(common::InvalidRequestException(err));
    delete request;
}

napi_value Server::SetSetCB(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value argv[1];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    Server *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, argv[0], &type));
    if (type == napi_undefined || type == napi_null || type == napi_function)
    {
        if (obj->m_setJSCallback)
            NAPI_CALL(napi_delete_reference(env, obj->m_setJSCallback));
        obj->m_setJSCallback = nullptr;
        if (type == napi_function)
        {
            NAPI_CALL(
                napi_create_reference(env, argv[0], 1, &obj->m_setJSCallback));
        }
    }
    napi_value ret;
    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

void Server::setJSReplyCallback(napi_env env,
                                napi_value /* js_callback */,
                                void *context,
                                void *vdata)
{
    Server *obj = reinterpret_cast<Server *>(context);
    std::unique_ptr<server::SetRequest> *request =
        reinterpret_cast<std::unique_ptr<server::SetRequest> *>(vdata);
    napi_status status;
    std::string err;

    if (!request)
        return;
    if (!obj->m_setJSCallback)
        err = "No setter handler installed";
    else
    {
        napi_value global;
        napi_get_global(env, &global);

        // Build the context data
        napi_value argv[1];
        NAPI_CALL(createRequestObject(env, request->get(), &argv[0]));

        // Add data to it
        napi_value setData = Data::NewInstance(env, (*request)->getData());
        NAPI_CALL(napi_set_named_property(env, argv[0], "Data", setData));

        napi_value func;
        NAPI_CALL(napi_get_reference_value(env, obj->m_setJSCallback, &func));
        napi_value cbRet;

        status = napi_call_function(env, global, func, 1, argv, &cbRet);
        if (status == napi_pending_exception)
        {
            napi_value ex;
            NAPI_CALL(napi_get_and_clear_last_exception(env, &ex));
            bool isError;
            NAPI_CALL(napi_is_error(env, ex, &isError));
            if (isError)
            {
                napi_value msg;
                NAPI_CALL(napi_get_named_property(env, ex, "message", &msg));
                NAPIUtils::getUTF8String(env, err, msg);
            }
            else
                err = "unknown error";
        }
        else
        {
            napi_valuetype retType;
            NAPI_CALL(napi_typeof(env, cbRet, &retType));
            if (retType == napi_string)
                NAPIUtils::getUTF8String(env, err, cbRet);
        }
    }

    if (err.empty())
        (*request)->requestCompleted();
    else
        (*request)->requestFailed(common::InvalidRequestException(err));
    delete request;
}

napi_value Server::SetSubscribeCB(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value argv[1];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    Server *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, argv[0], &type));
    if (type == napi_undefined || type == napi_null || type == napi_function)
    {
        if (obj->m_subJSCallback)
        {
            NAPI_CALL(napi_delete_reference(env, obj->m_subJSCallback));
        }
        obj->m_subJSCallback = nullptr;
        if (type == napi_function)
        {
            NAPI_CALL(
                napi_create_reference(env, argv[0], 1, &obj->m_subJSCallback));
        }
    }
    napi_value ret;
    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value Server::SetUnsubscribeCB(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value argv[1];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    Server *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, argv[0], &type));
    if (type == napi_undefined || type == napi_null || type == napi_function)
    {
        if (obj->m_unsubJSCallback)
        {
            NAPI_CALL(napi_delete_reference(env, obj->m_unsubJSCallback));
        }
        obj->m_unsubJSCallback = nullptr;
        if (type == napi_function)
        {
            NAPI_CALL(napi_create_reference(env, argv[0], 1,
                                            &obj->m_unsubJSCallback));
        }
    }
    napi_value ret;
    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value Server::Server::SetSubSrcAddedCB(napi_env env,
                                            napi_callback_info info)
{
    size_t argc = 1;
    napi_value argv[1];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    Server *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, argv[0], &type));
    if (type == napi_undefined || type == napi_null || type == napi_function)
    {
        if (obj->m_subSrcAddedCallback)
            NAPI_CALL(napi_delete_reference(env, obj->m_subSrcAddedCallback));
        obj->m_subSrcAddedCallback = nullptr;
        if (type == napi_function)
        {
            NAPI_CALL(napi_create_reference(env, argv[0], 1,
                                            &obj->m_subSrcAddedCallback));
        }
    }
    napi_value ret;
    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value Server::SetSubSrcRemovedCB(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value argv[1];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    Server *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, argv[0], &type));
    if (type == napi_undefined || type == napi_null || type == napi_function)
    {
        if (obj->m_subSrcRemovedCallback)
            NAPI_CALL(napi_delete_reference(env, obj->m_subSrcRemovedCallback));
        obj->m_subSrcRemovedCallback = nullptr;
        if (type == napi_function)
        {
            NAPI_CALL(napi_create_reference(env, argv[0], 1,
                                            &obj->m_subSrcRemovedCallback));
        }
    }
    napi_value ret;
    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

void Server::subJSReplyCallback(napi_env env,
                                napi_value /* js_callback */,
                                void *context,
                                void *vdata)
{
    Server *obj = reinterpret_cast<Server *>(context);
    std::shared_ptr<ServerRequestCallback> *shared =
        reinterpret_cast<std::shared_ptr<ServerRequestCallback> *>(vdata);
    ServerRequestCallback &req = **shared;

    std::unique_lock<std::mutex> lock(req.m);

    req.cb(*obj, req, env);

    req.notified = true;
    req.cond_var.notify_one();
    delete shared;
}

void Server::subscribeCallback(Server &obj,
                               ServerRequestCallback &req,
                               napi_env env)
{
    if (req.notified)
        return; /* in is already released, do nothing */

    const server::SubscriptionRequest *request =
        static_cast<const server::SubscriptionRequest *>(req.in);

    if (obj.m_subJSCallback)
    {
        napi_value global;
        napi_get_global(env, &global);

        napi_value argv[1];
        NAPI_CALL(createRequestObject(env, request, &argv[0]));

        napi_value func;
        NAPI_CALL(napi_get_reference_value(env, obj.m_subJSCallback, &func));
        napi_value cbRet;
        // Call the JS callback function
        napi_status status = napi_call_function(env, global, func, 1, argv,
                                                &cbRet);
        if (status == napi_pending_exception)
        {
            napi_value ex;
            NAPI_CALL(napi_get_and_clear_last_exception(env, &ex));
            bool isError;
            NAPI_CALL(napi_is_error(env, ex, &isError));
            if (isError)
            {
                napi_value msg;
                NAPI_CALL(napi_get_named_property(env, ex, "message", &msg));
                NAPIUtils::getUTF8String(env, req.errMsg, cbRet);
            }
        }
        else
        {
            napi_valuetype retType;
            NAPI_CALL(napi_typeof(env, cbRet, &retType));
            if (retType == napi_string)
            {
                NAPIUtils::getUTF8String(env, req.errMsg, cbRet);
            }
            else
            {
                bool is_instance = false;
                NAPI_CALL(napi_instanceof(env, cbRet,
                                          AcquiredData::GetConstructor(env),
                                          &is_instance));
                if (!is_instance)
                {
                    req.errMsg = "Argument is not a AcquiredData object";
                }
                else
                {
                    AcquiredData *data;
                    NAPI_CALL(napi_unwrap(env, cbRet,
                                          reinterpret_cast<void **>(&data)));
                    req.out = new common::AcquiredData(
                        data->getData(env)->cloneData(), data->getContext());
                }
            }
        }
    }
    else
    {
        req.errMsg = "No subscribe handler installed";
    }
}

void Server::unsubscribeCallback(Server &obj,
                                 ServerRequestCallback &req,
                                 napi_env env)
{
    if (req.notified)
        return; /* in is already released, do nothing */

    const common::Request *request = static_cast<const common::Request *>(
        req.in);
    const common::Request::Header &header = request->getHeader();

    if (obj.m_unsubJSCallback)
    {
        napi_value global;
        napi_get_global(env, &global);

        napi_value argv[1];
        NAPI_CALL(napi_create_object(env, &argv[0]));
        NAPI_CALL(NAPIUtils::addPropertyToObject(
            env, header.getAccessPointName(), "AccessPoint", argv[0]));
        NAPI_CALL(NAPIUtils::addPropertyToObject(env, header.getDeviceName(),
                                                 "Device", argv[0]));
        NAPI_CALL(NAPIUtils::addPropertyToObject(env, header.getPropertyName(),
                                                 "Property", argv[0]));
        NAPI_CALL(
            NAPIUtils::addPropertyToObject(env, header.getId(), "Id", argv[0]));

        napi_value func;
        NAPI_CALL(napi_get_reference_value(env, obj.m_unsubJSCallback, &func));
        napi_value cbRet;
        // Call the JS callback function
        napi_call_function(env, global, func, 1, argv, &cbRet);
        // No function return nor exception is expected here
    }
}

void Server::subSrcChangedCallback(Server &obj,
                                   ServerRequestCallback &req,
                                   napi_env env)
{
    if (req.notified)
        return; /* in is already released, do nothing */

    const server::SubscriptionSourceSharedPtr *src =
        static_cast<const server::SubscriptionSourceSharedPtr *>(req.in);
    napi_value global;
    napi_get_global(env, &global);

    if (req.evt == ServerRequestCallback::NEW_SOURCE)
    {
        napi_value arg;
        if (obj.m_subSrcAddedCallback &&
            createSubscriptionSourceObject(env, *src, true, arg) == napi_ok)
        {
            napi_value func;
            NAPI_CALL(napi_get_reference_value(env, obj.m_subSrcAddedCallback,
                                               &func));
            napi_value cbRet;
            // Call the JS callback function
            napi_call_function(env, global, func, 1, &arg, &cbRet);
        }
    }
    else if (req.evt == ServerRequestCallback::REM_SOURCE)
    {
        napi_value arg;
        if (obj.m_subSrcRemovedCallback &&
            createSubscriptionSourceObject(env, *src, false, arg) == napi_ok)
        {
            napi_value func;
            NAPI_CALL(napi_get_reference_value(env, obj.m_subSrcRemovedCallback,
                                               &func));
            napi_value cbRet;
            // Call the JS callback function
            napi_call_function(env, global, func, 1, &arg, &cbRet);
        }
    }
}

napi_status Server::createSubscriptionSourceObject(
    napi_env env,
    const server::SubscriptionSourceSharedPtr &sub,
    bool newSub,
    napi_value &result)
{
    napi_status ret;
    ret = napi_create_object(env, &result);
    if (ret != napi_ok)
        return ret;

    ret = NAPIUtils::addPropertyToObject(env, sub->getAccessPointName(),
                                         "AccessPoint", result);
    if (ret != napi_ok)
        return ret;
    ret = NAPIUtils::addPropertyToObject(env, sub->getDeviceName(), "Device",
                                         result);
    if (ret != napi_ok)
        return ret;
    ret = NAPIUtils::addPropertyToObject(env, sub->getPropertyName(),
                                         "Property", result);
    if (ret != napi_ok)
        return ret;
    ret = NAPIUtils::addPropertyToObject(env, sub->getId(), "Id", result);
    if (ret != napi_ok)
        return ret;

    if (newSub)
    {
        std::weak_ptr<server::SubscriptionSource> *weak(
            new std::weak_ptr<server::SubscriptionSource>(sub));
        napi_value notify;
        ret = napi_create_function(env, "notify", 6, Server::notify, weak,
                                   &notify);
        if (ret != napi_ok)
            return ret;

        NAPI_CALL(
            napi_wrap(env, notify, weak, finalize_notify, nullptr, nullptr));

        ret = napi_set_named_property(env, result, "notify", notify);
    }
    return ret;
}

napi_status Server::createRequestObject(napi_env env,
                                        const server::ServerRequest *req,
                                        napi_value *result)
{
    napi_status ret;
    ret = napi_create_object(env, result);
    if (ret != napi_ok)
        return ret;
    ret = NAPIUtils::addPropertyToObject(env, req->getAccessPointName(),
                                         "AccessPoint", *result);
    if (ret != napi_ok)
        return ret;
    ret = NAPIUtils::addPropertyToObject(env, req->getDeviceName(), "Device",
                                         *result);
    if (ret != napi_ok)
        return ret;
    ret = NAPIUtils::addPropertyToObject(env, req->getPropertyName(),
                                         "Property", *result);
    if (ret != napi_ok)
        return ret;
    ret = NAPIUtils::addPropertyToObject(env, req->getId(), "Id", *result);
    if (ret != napi_ok)
        return ret;
    napi_value jsContext;
    ret = createRequestContextObject(env, req->getContext(), &jsContext);
    if (ret != napi_ok)
        return ret;
    ret = napi_set_named_property(env, *result, "Context", jsContext);
    return ret;
}

napi_status Server::createRequestContextObject(
    napi_env env,
    const common::RequestContext &context,
    napi_value *result)
{
    napi_status ret;
    ret = napi_create_object(env, result);
    if (ret != napi_ok)
        return ret;
    ret = NAPIUtils::addPropertyToObject(env, context.getSelector(), "Selector",
                                         *result);
    if (ret != napi_ok)
        return ret;
    napi_value filters = Data::NewInstance(env, context.getFilters());
    ret = napi_set_named_property(env, *result, "Filters", filters);
    if (ret != napi_ok)
        return ret;
    napi_value genData = Data::NewInstance(env, context.getData());
    ret = napi_set_named_property(env, *result, "Data", genData);
    return ret;
}

napi_value Server::notify(napi_env env, napi_callback_info info)
{
    napi_value ret;
    NAPI_CALL(napi_get_undefined(env, &ret));
    size_t argc = 1;
    napi_value argv[1];
    std::weak_ptr<server::SubscriptionSource> *weak;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, nullptr,
                               reinterpret_cast<void **>(&weak)));

    server::SubscriptionSourceSharedPtr sub(weak->lock());
    if (!sub)
    {
        NAPI_CALL(
            napi_throw_error(env, "ESHUTDOWN", "Invalid subscription source"));
        return ret;
    }

    bool is_instance = false;
    NAPI_CALL(napi_instanceof(env, argv[0], AcquiredData::GetConstructor(env),
                              &is_instance));
    if (!is_instance)
    {
        NAPI_CALL(napi_throw_type_error(env, "EINVAL",
                                        "AcquiredData object expected"));
        return ret;
    }
    AcquiredData *data;
    NAPI_CALL(napi_unwrap(env, argv[0], reinterpret_cast<void **>(&data)));
    cmw::rda3::common::AcquiredData sData(data->getData(env)->cloneData(),
                                          data->getContext());
    if (sub)
        sub->notify(sData);
    else
        NAPI_CALL(
            napi_throw_error(env, "ESHUTDOWN", "Invalid subscription source"));
    return ret;
}

void Server::finalize_notify(napi_env env, void *data, void *hint)
{
    delete reinterpret_cast<std::weak_ptr<server::SubscriptionSource> *>(data);
}
