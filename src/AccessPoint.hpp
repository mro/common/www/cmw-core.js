/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#ifndef RDA3_ACCESS_POINT_
#define RDA3_ACCESS_POINT_

#include <condition_variable>
#include <memory>
#include <mutex>
#include <thread>

#include <cmw-fwk/client/rbac/RbacLoginService.h>
#include <cmw-rda3/client/service/ClientService.h>
#include <node_api.h>

class RDA3NotificationListener;
class RBACConfiguration;

class AccessPoint
{
public:
    static napi_value Init(napi_env env, napi_value exports);
    static void Destructor(napi_env env,
                           void *nativeObject,
                           void *finalize_hint);

    cmw::rda3::client::AccessPoint &getAccessPoint();
    /**
     * Adds a Javascript reference to this object
     */
    void addReference(napi_env env);

    /**
     * Removes a Javascript reference on this object
     */
    void removeReference(napi_env env);

    /**
     * @brief unsubscribe the AccessPoint
     * @details called from destructor
     */
    void unsubscribe();

private:
    explicit AccessPoint(napi_env env,
                         napi_value jsthis,
                         const std::string &device,
                         const std::string &property,
                         const std::string &server = "");
    ~AccessPoint();
    void Delete(napi_env env);

protected:
    static napi_value New(napi_env env, napi_callback_info info);

    static napi_value subscribe(napi_env env, napi_callback_info info);
    static napi_value unsubscribe(napi_env env, napi_callback_info info);
    static napi_value get(napi_env env, napi_callback_info info);
    static napi_value set(napi_env env, napi_callback_info info);
    static napi_value newData(napi_env env, napi_callback_info info);

    static napi_value setRBACConfiguration(napi_env env,
                                           napi_callback_info info);
    static napi_value getRBACConfiguration(napi_env env,
                                           napi_callback_info info);

    static napi_value reset(napi_env env, napi_callback_info info);

    static void jsReplyCallback(napi_env env,
                                napi_value js_callback,
                                void *context,
                                void *vdata);

    static napi_ref constructor;
    napi_ref m_wrapper;

    napi_ref m_rbacConfigRef;
    RBACConfiguration *m_rbacCfg;
    const std::string m_device;
    const std::string m_property;
    const std::string m_server;

    std::shared_ptr<RDA3NotificationListener> m_listener;
    cmw::rda3::client::SubscriptionSharedPtr m_sub;
};

class RDA3NotificationListener : public cmw::rda3::client::NotificationListener
{
public:
    RDA3NotificationListener() = default;
    virtual ~RDA3NotificationListener() = default;

    void dataReceived(const cmw::rda3::client::Subscription &subscription,
                      std::unique_ptr<cmw::rda3::common::AcquiredData> acqData,
                      cmw::rda3::common::UpdateType updateType) override;

    void errorReceived(const cmw::rda3::client::Subscription &subscription,
                       std::unique_ptr<cmw::rda3::common::RdaException> exception,
                       cmw::rda3::common::UpdateType updateType) override;

    void clear();

protected:
    std::mutex m_mutex;
    napi_threadsafe_function m_func;

    friend class AccessPoint;
};

#endif