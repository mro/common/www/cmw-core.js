/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#ifndef RDA3_ASYNC_GETTER_
#define RDA3_ASYNC_GETTER_

#include <cmw-rda3/client/service/ClientService.h>
#include <node_api.h>

#include "AccessPoint.hpp"

class AsyncGetter
{
public:
    static napi_value NewGetterPromise(
        napi_env env,
        AccessPoint *ap,
        const std::string &selector,
        std::unique_ptr<cmw::data::Data> context_data);

private:
    AsyncGetter(napi_env env,
                AccessPoint *ap,
                const std::string &selector,
                std::unique_ptr<cmw::data::Data> &context_data);
    virtual ~AsyncGetter();
    void Delete(napi_env);

protected:
    static void ExecuteTask(napi_env env, void *data);
    static void TaskCompleted(napi_env env, napi_status status, void *data);

    AccessPoint *m_ap;
    std::unique_ptr<cmw::rda3::common::AcquiredData> m_data;
    std::unique_ptr<cmw::data::Data> m_contextData;
    std::string m_selector;
    napi_async_work m_worker;
    napi_deferred m_deferred;
    std::string m_error;
};

#endif