
import EventEmitter from 'events';

export = cmwcore;
export as namespace cmwcore;

declare namespace cmwcore {
  export class AccessPoint extends EventEmitter {
    constructor(device: string, property: string, server?: string)
    subscribe(selector?: string, contextData?: Data): void
    unsubscribe(): void
    get(selector?: string, contextData?: Data): Promise<AcquiredData>
    // returns "done"
    set(data: Data, selector?: string, contextData?: Data): Promise<string>
    createData(): Data
    setRACConfiguration(config: RBACConfiguration): void
    getRBACConfiguration(): RBACConfiguration|undefined

    /**
     * @brief reset AccessPoint underlying service
     * @details useful in tests, to reset transports and factories
     */
    static reset(): undefined
  }

  export type DataValue = boolean|number|string|BigInt|DataValue[]

  export class Data {
    append(name: string, value: DataValue, type?: Data.NumberType|string): Data
    remove(name: string): void
    clear(): void
    getDataType(name: string): string|undefined
    setBigIntMode(mode: Data.BigIntOutputType|string): Data
    // FIXME: argument is not really used, it changes output type
    getBigIntMode(name?: string): Data.BigIntOutputType|string
    setNumberArrayMode(mode: Data.NumberArrayOutputType|string): Data
    // FIXME: argument is not really used, it changes output type
    getNumberArrayMode(name?: string): Data.NumberArrayOutputType|string

    entries?: { [property: string]: DataValue }
  }

  export namespace Data {
    enum NumberType { Double, Float, Int64, Int32, Int16, Int8 }
    enum BigIntOutputType { BigInt, String, Timestamp }
    enum NumberArrayOutputType { Regular, TypedArray }
  }

  export class AcquiredData {
    cycleName: string
    cycleStamp: BigInt
    acqStamp: BigInt
    data: Data
  }

  export class RBACConfiguration {
    setLoginPolicy(policy: RBACConfiguration.LoginPolicy|string): void
    // FIXME: argument is not really used, it changes output type
    getLoginPolicy(name?: string): RBACConfiguration.LoginPolicy|string
    setLoginName(name: string): void
    getLoginName(): string
    setPassword(password: string): void
    getApplicationName(): string
    setApplicationName(name: string): void

    authenticationDone?: (token: RBACConfiguration.AuthToken) => any
    authenticationError?: (message: string) => any
    authenticationExpired?: (token: RBACConfiguration.AuthToken) => any
  }

  export namespace RBACConfiguration {
    enum LoginPolicy { EXPLICIT, DEFAULT, LOCATION }
    interface AuthToken {
      signatureVerified: boolean
      tokenExpired: boolean
      serialID: number
      authenticationTime: number
      expirationTime: number
      applicationName: string
      locationName: string
      userName: string
      roles: string[]
      fullName: string
      email: string
      accountType: string
    }
  }

  export class Server {
    constructor(serverName: string)
    start(): void
    stop(): void
    addConfigFile(file: string): void
    setConfigProp(properties: { [key: string]: any }): void
    setGetCallback(cb: (req: Server.Request) => any): void
    setSetCallback(cb: (req: Server.Request) => any): void
    setSubscribeCallback(cb: (req: Server.Request) => AcquiredData): void
    setUnsubscribeCallback(cb: (info: { AccessPoint: string, Device: string, Property: string, Id: number }) => any): void
    setSubSrcAddedCallback(cb: (source: Server.SubscriptionSource) => any): void
    setSubSrcRemovedCallback(cb: (source: Server.SubscriptionSource) => any): void
  }
  export namespace Server {
    interface SubscriptionSource {
      AccessPoint: string,
      Device: string,
      Property: string,
      Id: number,

      notify(data: AcquiredData):  void
    }
    interface Request {
      AccessPoint: string,
      Device: string,
      Property: string,
      Id: number,
      Context: { Selector: string, Filters: Data, Data: Data },
      Data?: Data
    }
  }
}

