/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#include "NAPIUtils.hpp"

napi_status NAPIUtils::getUTF8String(napi_env env,
                                     std::string &str,
                                     napi_value value)
{
    napi_status status;

    size_t size;
    status = napi_get_value_string_utf8(env, value, NULL, 0, &size);
    if (status != napi_ok)
    {
        return status;
    }

    str.reserve(size + 1);
    str.resize(size);
    status = napi_get_value_string_utf8(env, value, &str[0], size + 1, &size);
    return status;
}

napi_status NAPIUtils::int64ToJSString(napi_env env,
                                       int64_t value,
                                       napi_value *result)
{
    std::string str = std::to_string(value);
    napi_status ret;
    ret = napi_create_string_utf8(env, str.c_str(), NAPI_AUTO_LENGTH, result);
    return ret;
}

napi_status NAPIUtils::int64ToTimeStamp(napi_env env,
                                        int64_t value,
                                        napi_value *result)
{
    napi_status ret;
    ret = napi_create_object(env, result);
    if (ret != napi_ok)
        return ret;
    napi_value ms;
    int64_t millis = value / 1000000L;
    ret = napi_create_int64(env, millis, &ms);
    if (ret != napi_ok)
        return ret;
    napi_value nano;
    int64_t nanos = static_cast<int64_t>(value % 1000000L);
    ret = napi_create_int64(env, nanos, &nano);
    if (ret != napi_ok)
        return ret;
    ret = napi_set_named_property(env, *result, "ms", ms);
    if (ret != napi_ok)
        return ret;
    ret = napi_set_named_property(env, *result, "ns", nano);
    if (ret != napi_ok)
        return ret;
    return napi_ok;
}

napi_status NAPIUtils::addPropertyToObject(napi_env env,
                                           const std::string &val,
                                           const std::string &name,
                                           napi_value obj)
{
    napi_status ret;
    napi_value jsVal;
    ret = napi_create_string_utf8(env, val.c_str(), NAPI_AUTO_LENGTH, &jsVal);
    if (ret != napi_ok)
        return ret;
    ret = napi_set_named_property(env, obj, name.c_str(), jsVal);
    return ret;
}

napi_status NAPIUtils::addPropertyToObject(napi_env env,
                                           int64_t val,
                                           const std::string &name,
                                           napi_value obj)
{
    napi_status ret;
    napi_value jsVal;
    ret = napi_create_int64(env, val, &jsVal);
    if (ret != napi_ok)
        return ret;
    ret = napi_set_named_property(env, obj, name.c_str(), jsVal);
    return ret;
}

bool NAPIUtils::caseInsensCompare(const std::string &str1,
                                  const std::string &str2)
{
    return ((str1.size() == str2.size()) &&
            std::equal(str1.begin(), str1.end(), str2.begin(),
                       [](const char &c1, const char &c2) {
                           return (c1 == c2 ||
                                   std::toupper(c1) == std::toupper(c2));
                       }));
}