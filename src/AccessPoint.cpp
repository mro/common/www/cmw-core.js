/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#include "AccessPoint.hpp"

#include <cassert>
#include <chrono>
#include <cstdint>
#include <iostream>
#include <string>
#include <thread>

#include <assert.h>
#include <cmw-fwk/client/rbac/RbacLoginService.h>
#include <cmw-rda3/client/service/ClientService.h>
#include <stdlib.h>

#include "AcquiredData.hpp"
#include "AsyncGetter.hpp"
#include "AsyncSetter.hpp"
#include "Data.hpp"
#include "NAPIUtils.hpp"
#include "RBACConfiguration.hpp"

using namespace cmw::rda3;

struct RDA3Payload
{
    std::unique_ptr<common::AcquiredData> acqData;
    common::UpdateType updateType;
    std::unique_ptr<common::RdaException> exception;
};

// This method is called when some data is received
void RDA3NotificationListener::dataReceived(
    const client::Subscription & /*subscription*/,
    std::unique_ptr<common::AcquiredData> acqData,
    common::UpdateType updateType)
{
    RDA3Payload *payload = new RDA3Payload();
    payload->acqData = std::move(acqData);
    payload->updateType = updateType;
    // Call the thread_safe_function
    std::lock_guard<std::mutex> lock(m_mutex);
    if (!m_func ||
        (napi_call_threadsafe_function(m_func, payload, napi_tsfn_blocking) !=
         napi_ok))
    {
        delete payload;
    }
}

// This method is called when an error is received
void RDA3NotificationListener::errorReceived(
    const client::Subscription & /* subscription */,
    std::unique_ptr<common::RdaException> exception,
    common::UpdateType /* updateType */)
{
    RDA3Payload *payload = new RDA3Payload();
    payload->exception = std::move(exception);
    // Call the thread_safe_function
    std::lock_guard<std::mutex> lock(m_mutex);
    if (!m_func ||
        (napi_call_threadsafe_function(m_func, payload, napi_tsfn_blocking) !=
         napi_ok))
    {
        delete payload;
    }
}

void RDA3NotificationListener::clear()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    if (m_func)
    {
        NAPI_CALL(napi_release_threadsafe_function(m_func, napi_tsfn_abort));
        m_func = nullptr;
    }
}

napi_ref AccessPoint::constructor;
static std::unique_ptr<client::ClientService> s_clientService;

static void DestructorNoGC(node_api_nogc_env env, void *data, void *hint)
{
#if defined(NAPI_EXPERIMENTAL)
    node_api_post_finalizer(env, AccessPoint::Destructor, data, hint);
#else
    AccessPoint::Destructor(env, data, hint);
#endif
}

AccessPoint::AccessPoint(napi_env env,
                         napi_value jsthis,
                         const std::string &device,
                         const std::string &property,
                         const std::string &server) :
    m_wrapper(nullptr),
    m_rbacConfigRef(nullptr),
    m_rbacCfg(nullptr),
    m_device(device),
    m_property(property),
    m_server(server)
{
    NAPI_CALL(napi_wrap(env, jsthis, reinterpret_cast<void *>(this),
                        DestructorNoGC,
                        nullptr, // finalize_hint
                        &m_wrapper));
}

AccessPoint::~AccessPoint()
{
    // use AccessPoint::Delete
    assert(m_wrapper == nullptr);
}

void AccessPoint::Destructor(napi_env env,
                             void *nativeObject,
                             void * /* finalize_hint */)
{
    reinterpret_cast<AccessPoint *>(nativeObject)->Delete(env);
}

napi_value AccessPoint::Init(napi_env env, napi_value /* exports */)
{
    if (!s_clientService)
        s_clientService = client::ClientService::create();

    napi_property_descriptor props[] = {
        DECLARE_NAPI_METHOD("subscribe", AccessPoint::subscribe),
        DECLARE_NAPI_METHOD("unsubscribe", AccessPoint::unsubscribe),
        DECLARE_NAPI_METHOD("get", AccessPoint::get),
        DECLARE_NAPI_METHOD("set", AccessPoint::set),
        DECLARE_NAPI_METHOD("createData", AccessPoint::newData),
        DECLARE_NAPI_METHOD("setRBACConfiguration",
                            AccessPoint::setRBACConfiguration),
        DECLARE_NAPI_METHOD("getRBACConfiguration",
                            AccessPoint::getRBACConfiguration),
        DECLARE_NAPI_STATIC_METHOD("reset", AccessPoint::reset)};

    napi_value cons;
    NAPI_CALL(napi_define_class(
        env, "AccessPoint", NAPI_AUTO_LENGTH, New, nullptr,
        sizeof(props) / sizeof(napi_property_descriptor), props, &cons));

    NAPI_CALL(napi_create_reference(env, cons, 1, &constructor));
    return cons;
}

napi_value AccessPoint::New(napi_env env, napi_callback_info info)
{
    napi_status status;

    napi_value target;
    NAPI_CALL(napi_get_new_target(env, info, &target));
    bool is_constructor = target != nullptr;

    if (is_constructor)
    {
        // Invoked as constructor: `new MyObject(...)`
        size_t argc = 3;
        napi_value argv[3];

        napi_value jsthis;
        NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

        struct
        {
            const char *name;
            std::string value;
        } args[] = {{"device", std::string()}, {"property", std::string()}};
        std::string serverName;

        if (argc < 2)
        {
            napi_throw_type_error(env, "EINVAL", "missing arguments");
            return jsthis;
        }
        else
        {
            for (size_t i = 0; i < 2; ++i)
            {
                status = NAPIUtils::getUTF8String(env, args[i].value, argv[i]);
                if (status != napi_ok)
                {
                    napi_throw_type_error(
                        env, "EINVAL",
                        (std::string("invalid") + args[i].name + " name")
                            .c_str());
                    return jsthis;
                }
            }
            if (argc > 2)
            {
                status = NAPIUtils::getUTF8String(env, serverName, argv[2]);
                if (status != napi_ok)
                {
                    napi_throw_type_error(env, "EINVAL", "invalid server name");
                    return jsthis;
                }
            }
        }

        new AccessPoint(env, jsthis, args[0].value, args[1].value, serverName);
        return jsthis;
    }
    else
    {
        napi_value cons;
        NAPI_CALL(napi_get_reference_value(env, constructor, &cons));

        napi_value instance;
        NAPI_CALL(napi_new_instance(env, cons, 0, nullptr, &instance));

        return instance;
    }
}

void AccessPoint::Delete(napi_env env)
{
    unsubscribe();
    napi_delete_reference(env, m_wrapper);
    m_wrapper = nullptr;
    napi_delete_reference(env, m_rbacConfigRef);
    m_rbacConfigRef = nullptr;
    delete this;
}

napi_value AccessPoint::subscribe(napi_env env, napi_callback_info info)
{
    napi_value jsthis;
    size_t argc = 2;
    napi_value argv[2];

    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    AccessPoint *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    napi_value ret;
    napi_get_undefined(env, &ret);

    if (!obj->m_listener)
    {
        RDA3NotificationListener *listener = new RDA3NotificationListener();

        // Create the threadsafe_function
        napi_value work_name;
        NAPI_CALL(napi_create_string_utf8(env, "RDA reply", NAPI_AUTO_LENGTH,
                                          &work_name));
        NAPI_CALL(napi_create_threadsafe_function(
            env, nullptr, nullptr, work_name, 0, 1, nullptr, nullptr,
            obj->m_wrapper, &AccessPoint::jsReplyCallback, &(listener->m_func)));
        obj->m_listener.reset(listener);
    }

    if (!obj->m_sub)
    {
        try
        {
            client::AccessPoint &AccessPoint = obj->getAccessPoint();

            napi_valuetype argType;
            NAPI_CALL(napi_typeof(env, argv[0], &argType));
            std::string selector;
            if ((argType != napi_undefined) && (argType != napi_null))
            {
                if (NAPIUtils::getUTF8String(env, selector, argv[0]) != napi_ok)
                {
                    napi_throw_type_error(env, "EINVAL", "Invalid selector");
                    return ret;
                }
            }

            bool is_instance = false;
            ::Data *context_data{nullptr};
            NAPI_CALL(napi_instanceof(env, argv[1], ::Data::GetConstructor(env),
                                      &is_instance));
            if (is_instance)
            {
                NAPI_CALL(napi_unwrap(
                    env, argv[1], reinterpret_cast<void **>(&context_data)));
            }

            std::unique_ptr<::common::RequestContext> rcontext{
                ::common::RequestContextFactory::create(
                    selector, std::unique_ptr<cmw::data::Data>(),
                    context_data ? std::move(context_data->cloneData()) :
                                   std::unique_ptr<cmw::data::Data>())};
            obj->m_sub = AccessPoint.subscribe(std::move(rcontext),
                                               obj->m_listener);
        }
        catch (const std::exception &ex)
        {
            napi_throw_error(env, "RDA3", ex.what());
        }
    }
    else
    {
        napi_throw_error(env, "EISCONN", "Already subscribed");
    }
    return ret;
}

napi_value AccessPoint::unsubscribe(napi_env env, napi_callback_info info)
{
    napi_value jsthis;

    NAPI_CALL(napi_get_cb_info(env, info, nullptr, nullptr, &jsthis, nullptr));

    AccessPoint *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    obj->unsubscribe();

    napi_value ret;
    napi_get_undefined(env, &ret);
    return ret;
}

void AccessPoint::unsubscribe()
{
    cmw::rda3::client::SubscriptionSharedPtr sub;
    m_sub.swap(sub);
    if (sub)
    {
        sub->unsubscribe();
    }
    if (m_listener)
    {
        m_listener->clear();
        m_listener.reset();
    }
}

client::AccessPoint &AccessPoint::getAccessPoint()
{
    if (m_rbacCfg)
    {
        if (m_server.empty())
            return m_rbacCfg->getClientService()->getAccessPoint(m_device,
                                                                 m_property);

        return m_rbacCfg->getClientService()->getAccessPoint(
            m_device, m_property, m_server);
    }

    if (m_server.empty())
        return s_clientService->getAccessPoint(m_device, m_property);

    return s_clientService->getAccessPoint(m_device, m_property, m_server);
}

napi_value AccessPoint::get(napi_env env, napi_callback_info info)
{
    napi_value jsthis;
    size_t argc = 2;
    napi_value argv[2];

    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    AccessPoint *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    napi_valuetype argType;
    NAPI_CALL(napi_typeof(env, argv[0], &argType));
    std::string selector;
    if ((argType != napi_undefined) && (argType != napi_null))
    {
        if (NAPIUtils::getUTF8String(env, selector, argv[0]) != napi_ok)
        {
            napi_throw_type_error(env, "EINVAL", "Invalid selector");
            napi_value ret;
            napi_get_undefined(env, &ret);
            return ret;
        }
    }

    bool is_instance = false;
    ::Data *context_data{nullptr};
    NAPI_CALL(napi_instanceof(env, argv[1], ::Data::GetConstructor(env),
                              &is_instance));
    if (is_instance)
    {
        NAPI_CALL(napi_unwrap(env, argv[1],
                              reinterpret_cast<void **>(&context_data)));
    }

    return AsyncGetter::NewGetterPromise(
        env, obj, selector,
        context_data ? context_data->cloneData() :
                       std::unique_ptr<cmw::data::Data>());
}

void AccessPoint::addReference(napi_env env)
{
    uint32_t refCount;
    NAPI_CALL(napi_reference_ref(env, m_wrapper, &refCount));
}

void AccessPoint::removeReference(napi_env env)
{
    uint32_t refCount;
    NAPI_CALL(napi_reference_unref(env, m_wrapper, &refCount));
}

napi_value AccessPoint::set(napi_env env, napi_callback_info info)
{
    size_t argc = 3;
    napi_value argv[3];

    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));
    if (argc < 1)
    {
        napi_throw_error(env, "EINVAL", "Missing data");
        return jsthis;
    }
    bool is_instance = false;
    NAPI_CALL(napi_instanceof(env, argv[0], ::Data::GetConstructor(env),
                              &is_instance));
    if (!is_instance)
    {
        napi_throw_error(env, "EINVAL", "Argument is not a Data object");
        return jsthis;
    }

    AccessPoint *ap;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&ap)));

    ::Data *data;
    NAPI_CALL(napi_unwrap(env, argv[0], reinterpret_cast<void **>(&data)));

    // Get type of second argument
    napi_valuetype argType;
    NAPI_CALL(napi_typeof(env, argv[1], &argType));
    std::string selector;
    if ((argType != napi_undefined) && (argType != napi_null))
    {
        if (NAPIUtils::getUTF8String(env, selector, argv[1]) != napi_ok)
        {
            napi_throw_type_error(env, "EINVAL", "Invalid selector");
            napi_value ret;
            napi_get_undefined(env, &ret);
            return ret;
        }
    }

    ::Data *context_data{nullptr};
    NAPI_CALL(napi_instanceof(env, argv[2], ::Data::GetConstructor(env),
                              &is_instance));
    if (is_instance)
    {
        NAPI_CALL(napi_unwrap(env, argv[2],
                              reinterpret_cast<void **>(&context_data)));
    }

    return AsyncSetter::NewSetterPromise(
        env, ap, data->cloneData(), selector,
        context_data ? context_data->cloneData() :
                       std::unique_ptr<cmw::data::Data>());
}

napi_value AccessPoint::newData(napi_env env, napi_callback_info /* info */)
{
    napi_value ret;
    NAPI_CALL(::Data::NewInstance(env, &ret));
    return ret;
}

void AccessPoint::jsReplyCallback(napi_env env,
                                  napi_value /* js_callbackb */,
                                  void *context,
                                  void *vdata)
{
    // Called in JS context
    napi_ref wrapper = reinterpret_cast<napi_ref>(context);
    RDA3Payload *data = reinterpret_cast<RDA3Payload *>(vdata);

    napi_status status;
    napi_value jsthis;
    status = napi_get_reference_value(env, wrapper, &jsthis);
    if ((status == napi_ok) && (jsthis != nullptr))
    {
        napi_value emit;
        NAPI_CALL(napi_get_named_property(env, jsthis, "emit", &emit));

        napi_value argv[2];
        if (data->acqData)
        {
            NAPI_CALL(napi_create_string_utf8(env, "data", NAPI_AUTO_LENGTH,
                                              &argv[0]));
            argv[1] = ::AcquiredData::NewInstance(env, data->acqData);
            napi_value updateType;
            NAPI_CALL(napi_create_string_utf8(
                env, common::toString(data->updateType).c_str(),
                NAPI_AUTO_LENGTH, &updateType));
            NAPI_CALL(napi_set_named_property(env, argv[1], "updateType",
                                              updateType));
        }
        else
        {
            NAPI_CALL(napi_create_string_utf8(env, "error", NAPI_AUTO_LENGTH,
                                              &argv[0]));
            NAPI_CALL(napi_create_string_utf8(env, data->exception->what(),
                                              NAPI_AUTO_LENGTH, &argv[1]));
        }
        napi_value ret;
        status = napi_call_function(env, jsthis, emit, 2, argv, &ret);
        if (status == napi_pending_exception)
        {
            napi_value ex;
            NAPI_CALL(napi_get_and_clear_last_exception(env, &ex));
            bool isError;
            NAPI_CALL(napi_is_error(env, ex, &isError));
            if (isError)
            {
                napi_value msg;
                NAPI_CALL(napi_get_named_property(env, ex, "message", &msg));
                std::string msgStr;
                NAPIUtils::getUTF8String(env, msgStr, msg);
                NAPI_CALL(napi_throw(env, ex));
            }
        }
    }
    delete data;
}

napi_value AccessPoint::setRBACConfiguration(napi_env env,
                                             napi_callback_info info)
{
    napi_value ret;
    size_t argc = 1;
    napi_value argv[1];

    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));
    if (argc < 1)
    {
        napi_throw_error(env, "EINVAL", "Missing configuration");
        return jsthis;
    }
    bool is_instance = false;
    NAPI_CALL(napi_instanceof(
        env, argv[0], RBACConfiguration::GetConstructor(env), &is_instance));
    if (!is_instance)
    {
        napi_throw_error(env, "EINVAL",
                         "Argument is not a RBACConfiguration object");
        return jsthis;
    }

    AccessPoint *ap;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&ap)));

    NAPI_CALL(
        napi_unwrap(env, argv[0], reinterpret_cast<void **>(&ap->m_rbacCfg)));
    NAPI_CALL(napi_create_reference(env, argv[0], 1, &ap->m_rbacConfigRef));
    // TODO: Reconnect subscription if active
    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value AccessPoint::getRBACConfiguration(napi_env env,
                                             napi_callback_info info)
{
    size_t argc = 0;
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, nullptr, &jsthis, nullptr));

    AccessPoint *ap;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&ap)));

    napi_value ret;
    if (ap->m_rbacConfigRef)
    {
        NAPI_CALL(napi_get_reference_value(env, ap->m_rbacConfigRef, &ret));
    }
    else
    {
        NAPI_CALL(napi_get_undefined(env, &ret));
    }
    return ret;
}

napi_value AccessPoint::reset(napi_env env, napi_callback_info info)
{
    s_clientService = client::ClientService::create();

    napi_value ret;
    napi_get_undefined(env, &ret);
    return ret;
}
