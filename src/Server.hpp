/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#ifndef RDA3SERVER_H
#define RDA3SERVER_H

#include <condition_variable>
#include <functional>
#include <map>
#include <mutex>

#include <cmw-rda3/server/service/ServerBuilder.h>
#include <cmw-util/config/ConfigurationBuilder.h>
#include <node_api.h>

class Server :
    public cmw::rda3::server::RequestReplyCallback,
    public cmw::rda3::server::SubscriptionCallback
{
public:
    static napi_value Init(napi_env env, napi_value exports);
    static void Destructor(napi_env env,
                           void *nativeObject,
                           void *finalize_hint);
    static napi_status NewInstance(napi_env env, napi_value *instance);

    static napi_value GetConstructor(napi_env env);
    static napi_value Start(napi_env env, napi_callback_info info);
    static napi_value Stop(napi_env env, napi_callback_info info);
    static napi_value AddConfigurationFile(napi_env env,
                                           napi_callback_info info);
    static napi_value SetConfigurationProperty(napi_env env,
                                               napi_callback_info info);
    static napi_value SetGetCB(napi_env env, napi_callback_info info);
    static napi_value SetSetCB(napi_env env, napi_callback_info info);
    static napi_value SetSubscribeCB(napi_env env, napi_callback_info info);
    static napi_value SetUnsubscribeCB(napi_env env, napi_callback_info info);
    static napi_value SetSubSrcAddedCB(napi_env env, napi_callback_info info);
    static napi_value SetSubSrcRemovedCB(napi_env env, napi_callback_info info);

    void get(std::unique_ptr<cmw::rda3::server::GetRequest> request) override;
    void set(std::unique_ptr<cmw::rda3::server::SetRequest> request) override;

    void subscribe(cmw::rda3::server::SubscriptionRequest &request) override;
    void unsubscribe(const cmw::rda3::common::Request &request) override;
    void subscriptionSourceAdded(
        const cmw::rda3::server::SubscriptionSourceSharedPtr &subscription)
        override;
    void subscriptionSourceRemoved(
        const cmw::rda3::server::SubscriptionSourceSharedPtr &subscription)
        override;

    bool Stop();

private:
    Server(const std::string &serverName);
    ~Server();
    void Delete(napi_env env);

protected:
    struct ServerRequestCallback
    {
        enum event
        {
            SUBSCRIBE,
            UNSUBSCRIBE,
            NEW_SOURCE,
            REM_SOURCE
        };
        std::mutex m;
        std::condition_variable cond_var;
        bool notified;
        event evt;
        std::string errMsg;
        const void *in;
        void *out;
        std::function<void(Server &, ServerRequestCallback &, napi_env)> cb;
        ServerRequestCallback(
            event evt,
            const void *in,
            const std::function<void(Server &, ServerRequestCallback &, napi_env)>
                &cb) :
            notified(false), evt(evt), in(in), out(nullptr), cb(cb)
        {}
    };

    static napi_ref s_constructor;
    static napi_value New(napi_env env, napi_callback_info info);

    static void getJSReplyCallback(napi_env env,
                                   napi_value js_callback,
                                   void *context,
                                   void *vdata);
    static void setJSReplyCallback(napi_env env,
                                   napi_value js_callback,
                                   void *context,
                                   void *vdata);
    static void subJSReplyCallback(napi_env env,
                                   napi_value js_callback,
                                   void *context,
                                   void *vdata);
    static void subscribeCallback(Server &obj,
                                  ServerRequestCallback &cbInfo,
                                  napi_env env);
    static void unsubscribeCallback(Server &obj,
                                    ServerRequestCallback &cbInfo,
                                    napi_env env);
    static void subSrcChangedCallback(Server &obj,
                                      ServerRequestCallback &cbInfo,
                                      napi_env env);

    static napi_status createRequestObject(
        napi_env env,
        const cmw::rda3::server::ServerRequest *req,
        napi_value *result);

    static napi_status createRequestContextObject(
        napi_env env,
        const cmw::rda3::common::RequestContext &context,
        napi_value *result);

    static napi_status createSubscriptionSourceObject(
        napi_env env,
        const cmw::rda3::server::SubscriptionSourceSharedPtr &sub,
        bool newSub,
        napi_value &result);

    static napi_value notify(napi_env env, napi_callback_info info);
    static void finalize_notify(napi_env env, void *data, void *hint);

    std::unique_ptr<cmw::rda3::server::Server> m_server;
    std::unique_ptr<cmw::util::ConfigurationBuilder> m_configBuilder;
    const std::string m_serverName;

    std::mutex m_mutex;
    napi_threadsafe_function m_getTSFunc;
    napi_ref m_getJSCallback;
    napi_threadsafe_function m_setTSFunc;
    napi_ref m_setJSCallback;
    napi_threadsafe_function m_subTSFunc;
    napi_ref m_subJSCallback;
    napi_ref m_unsubJSCallback;
    napi_ref m_subSrcAddedCallback;
    napi_ref m_subSrcRemovedCallback;
    std::map<int64_t, cmw::rda3::server::SubscriptionSourceSharedPtr> m_subSources;
};

#endif // RDA3SERVER_H
