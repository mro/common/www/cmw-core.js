
const
  EventEmitter = require('events').EventEmitter,
  RDA3 = require('bindings')('RDA3'),
  inherits = require('util').inherits;


inherits(RDA3.AccessPoint, EventEmitter);

module.exports = RDA3;
