/*
** Copyright (C) 2024 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
*/

#include "RBACConfiguration.hpp"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <thread>

#include "NAPIUtils.hpp"

struct RBACAuthNotification
{
    std::unique_ptr<rbac::Token> newToken;
    std::string errMsg;
    std::unique_ptr<rbac::Token> oldToken;
};

void RBACConfiguration::authenticationDone(const rbac::Token &token)
{
    RBACAuthNotification *authNot = new RBACAuthNotification();
    authNot->newToken.reset(new rbac::Token(token));
    NAPI_CALL(napi_call_threadsafe_function(m_authChangedFunc, authNot,
                                            napi_tsfn_blocking));
}

void RBACConfiguration::authenticationError(
    const rbac::AuthenticationFailure &error)
{
    RBACAuthNotification *authNot = new RBACAuthNotification();
    authNot->errMsg = error.what();
    NAPI_CALL(napi_call_threadsafe_function(m_authChangedFunc, authNot,
                                            napi_tsfn_blocking));
}

void RBACConfiguration::tokenExpired(const rbac::Token &token)
{
    RBACAuthNotification *authNot = new RBACAuthNotification();
    authNot->oldToken.reset(new rbac::Token(token));
    NAPI_CALL(napi_call_threadsafe_function(m_authChangedFunc, authNot,
                                            napi_tsfn_blocking));
}

RBACConfiguration::RBACConfiguration() :
    m_wrapper(nullptr),
    m_loginPolicy(rbac::LOCATION),
    m_cfgNeeded(true),
    m_applicationName("cmw-core.js"),
    m_authChangedFunc(nullptr),
    m_authDoneJSFunc(nullptr),
    m_authErrorJSFunc(nullptr),
    m_authExpiredJSFunc(nullptr)
{}

RBACConfiguration::~RBACConfiguration()
{
    // use RBACConfiguration::Delete
    assert(m_wrapper == nullptr);
}

void RBACConfiguration::Destructor(napi_env env,
                                   void *nativeObject,
                                   void * /* finalize_hint */)
{
    reinterpret_cast<RBACConfiguration *>(nativeObject)->Delete(env);
}

napi_ref RBACConfiguration::s_constructor = nullptr;

napi_value RBACConfiguration::Init(napi_env env, napi_value /* exports */)
{
    napi_value loginPolicyTypes;
    NAPI_CALL(napi_create_object(env, &loginPolicyTypes));
    struct
    {
        const char *name;
        rbac::LoginPolicy type;
    } loginPolicyEnum[] = {{"EXPLICIT", rbac::EXPLICIT},
                           {"DEFAULT", rbac::DEFAULT},
                           {"LOCATION", rbac::LOCATION}};
    for (uint32_t i = 0; i < 3; ++i)
    {
        napi_value v;
        NAPI_CALL(napi_create_uint32(
            env, static_cast<uint32_t>(loginPolicyEnum[i].type), &v));
        NAPI_CALL(napi_set_named_property(env, loginPolicyTypes,
                                          loginPolicyEnum[i].name, v));
    }

    napi_property_descriptor properties[] = {
        DECLARE_NAPI_METHOD("setLoginPolicy", RBACConfiguration::SetLoginPolicy),
        DECLARE_NAPI_METHOD("getLoginPolicy", RBACConfiguration::GetLoginPolicy),
        DECLARE_NAPI_METHOD("setLoginName", RBACConfiguration::SetLoginName),
        DECLARE_NAPI_METHOD("getLoginName", RBACConfiguration::GetLoginName),
        DECLARE_NAPI_METHOD("setPassword", RBACConfiguration::SetPassword),
        DECLARE_NAPI_METHOD("getApplicationName",
                            RBACConfiguration::GetApplicationName),
        DECLARE_NAPI_METHOD("setApplicationName",
                            RBACConfiguration::SetApplicationName),
        DECLARE_NAPI_SETTER("authenticationDone",
                            RBACConfiguration::SetAuthDoneCB),
        DECLARE_NAPI_SETTER("authenticationError",
                            RBACConfiguration::SetAuthErrorCB),
        DECLARE_NAPI_SETTER("authenticationExpired",
                            RBACConfiguration::SetAuthExpiredCB),
        DECLARE_NAPI_VALUE_ATTR("LoginPolicy", loginPolicyTypes, napi_static),
    };

    napi_value cons;
    NAPI_CALL(napi_define_class(
        env, "RBACConfiguration", NAPI_AUTO_LENGTH, New, nullptr,
        sizeof(properties) / sizeof(napi_property_descriptor), properties,
        &cons));

    NAPI_CALL(napi_create_reference(env, cons, 1, &s_constructor));
    return cons;
}

napi_value RBACConfiguration::New(napi_env env, napi_callback_info info)
{
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, nullptr, nullptr, &jsthis, nullptr));

    RBACConfiguration *obj = new RBACConfiguration();
    NAPI_CALL(napi_wrap(env, jsthis, reinterpret_cast<void *>(obj),
                        RBACConfiguration::Destructor,
                        nullptr, /* finalize_hint */
                        &obj->m_wrapper));
    napi_value work_name;
    NAPI_CALL(napi_create_string_utf8(env, "RBAC configuration",
                                      NAPI_AUTO_LENGTH, &work_name));
    NAPI_CALL(napi_create_threadsafe_function(
        env, nullptr, nullptr, work_name, 0, 1, obj, nullptr, obj->m_wrapper,
        &RBACConfiguration::authChangedJSReplyCallback,
        &(obj->m_authChangedFunc)));

    // Define properties for this object
    /*napi_property_descriptor properties[] = {
        DECLARE_NAPI_GETTER_ATTR("entries", RDA3Data::Entries, napi_enumerable)
    };
    NAPI_CALL(napi_define_properties(env, jsthis, 1, properties));*/

    return jsthis;
}

void RBACConfiguration::Delete(napi_env env)
{
    NAPI_CALL(
        napi_release_threadsafe_function(m_authChangedFunc, napi_tsfn_abort));
    napi_delete_reference(env, m_wrapper);
    m_wrapper = nullptr;
    napi_delete_reference(env, m_authDoneJSFunc);
    m_authDoneJSFunc = nullptr;
    napi_delete_reference(env, m_authErrorJSFunc);
    m_authErrorJSFunc = nullptr;
    napi_delete_reference(env, m_authExpiredJSFunc);
    m_authExpiredJSFunc = nullptr;

    delete this;
}

napi_status RBACConfiguration::NewInstance(napi_env env, napi_value *instance)
{
    napi_status status;

    napi_value cons;
    cons = GetConstructor(env);
    status = napi_get_reference_value(env, s_constructor, &cons);
    if (status != napi_ok)
        return status;

    status = napi_new_instance(env, cons, 0, nullptr, instance);
    return status;
}

napi_value RBACConfiguration::GetConstructor(napi_env env)
{
    napi_value ret = NULL;
    NAPI_CALL(napi_get_reference_value(env, s_constructor, &ret));
    return ret;
}

void RBACConfiguration::reconfigure()
{
    const std::lock_guard<std::mutex> lock(m_mutex);
    if (m_cfgNeeded)
    {
        if (!m_clientSvc)
        {
            m_clientSvc = cmw::rda3::client::ClientService::create();
            m_rbacLoginSvc.reset(new cmw::RbacLoginService(*m_clientSvc));
            m_rbacLoginSvc->setAuthenticationListener(*this);
        }
        m_rbacLoginSvc->stop();
        m_rbacLoginSvc->setApplicationName(m_applicationName);
        m_rbacLoginSvc->setLoginPolicy(m_loginPolicy);
        m_rbacLoginSvc->setUserName(m_userName);
        m_rbacLoginSvc->setPassword(m_password);
        m_rbacLoginSvc->start();
        m_cfgNeeded = false;
    }
}

napi_value RBACConfiguration::GetLoginPolicy(napi_env env,
                                             napi_callback_info info)
{
    napi_value ret;
    size_t argc = 1;
    napi_value argv[1];

    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    RBACConfiguration *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    if (argc > 0)
    {
        napi_valuetype type;
        NAPI_CALL(napi_typeof(env, argv[0], &type));
        if (type == napi_string)
        {
            std::string loginPolicyStr;
            switch (obj->m_loginPolicy)
            {
            case rbac::EXPLICIT: loginPolicyStr = "EXPLICIT"; break;
            case rbac::LOCATION: loginPolicyStr = "LOCATION"; break;
            default: loginPolicyStr = "DEFAULT"; break;
            }
            NAPI_CALL(napi_create_string_utf8(env, loginPolicyStr.c_str(),
                                              NAPI_AUTO_LENGTH, &ret));
            return ret;
        }
    }
    NAPI_CALL(
        napi_create_int32(env, static_cast<int32_t>(obj->m_loginPolicy), &ret));
    return ret;
}

napi_value RBACConfiguration::SetLoginPolicy(napi_env env,
                                             napi_callback_info info)
{
    napi_value ret;
    size_t argc = 1;
    napi_value argv[1];

    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    RBACConfiguration *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    if (argc > 0)
    {
        napi_valuetype type;
        NAPI_CALL(napi_typeof(env, argv[0], &type));
        if (type == napi_number)
        {
            int32_t value;
            NAPI_CALL(napi_get_value_int32(env, argv[0], &value));
            if (value < static_cast<int32_t>(rbac::EXPLICIT) ||
                value > static_cast<int32_t>(rbac::LOCATION))
            {
                NAPI_CALL(napi_throw_range_error(env, "EINVAL",
                                                 "Policy value out of range"));
            }
            else
            {
                obj->m_loginPolicy = static_cast<rbac::LoginPolicy>(value);
                obj->m_cfgNeeded = true;
            }
        }
        else if (type == napi_string)
        {
            std::string argValue;
            NAPI_CALL(NAPIUtils::getUTF8String(env, argValue, argv[0]));
            std::transform(argValue.begin(), argValue.end(), argValue.begin(),
                           [](unsigned char c) { return std::toupper(c); });
            if (argValue == "EXPLICIT")
            {
                obj->m_loginPolicy = rbac::EXPLICIT;
                obj->m_cfgNeeded = true;
            }
            else if (argValue == "DEFAULT")
            {
                obj->m_loginPolicy = rbac::DEFAULT;
                obj->m_cfgNeeded = true;
            }
            else if (argValue == "LOCATION")
            {
                obj->m_loginPolicy = rbac::LOCATION;
                obj->m_cfgNeeded = true;
            }
            else
            {
                NAPI_CALL(napi_throw_error(env, "EINVAL", "Bad policy value"));
            }
        }
        else
        {
            NAPI_CALL(napi_throw_type_error(env, "EINVAL", "Bad argument type"));
        }
    }
    else
    {
        NAPI_CALL(napi_throw_type_error(env, "EINVAL", "Argument missing"));
    }
    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value RBACConfiguration::SetLoginName(napi_env env, napi_callback_info info)
{
    napi_value ret;
    size_t argc = 1;
    napi_value argv[1];

    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    RBACConfiguration *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, argv[0], &type));
    if (type == napi_string)
    {
        NAPI_CALL(NAPIUtils::getUTF8String(env, obj->m_userName, argv[0]));
        obj->m_cfgNeeded = true;
    }
    else
    {
        NAPI_CALL(napi_throw_type_error(env, "EINVAL", "Bad argument"));
    }

    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value RBACConfiguration::GetLoginName(napi_env env, napi_callback_info info)
{
    napi_value ret;
    size_t argc = 1;
    napi_value argv[1];

    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    RBACConfiguration *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    NAPI_CALL(napi_create_string_utf8(env, obj->m_userName.c_str(),
                                      NAPI_AUTO_LENGTH, &ret));
    return ret;
}

napi_value RBACConfiguration::SetPassword(napi_env env, napi_callback_info info)
{
    napi_value ret;
    size_t argc = 1;
    napi_value argv[1];

    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    RBACConfiguration *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, argv[0], &type));
    if (type == napi_string)
    {
        NAPI_CALL(NAPIUtils::getUTF8String(env, obj->m_password, argv[0]));
        obj->m_cfgNeeded = true;
    }
    else
    {
        NAPI_CALL(napi_throw_type_error(env, "EINVAL", "Bad argument"));
    }

    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value RBACConfiguration::SetApplicationName(napi_env env,
                                                 napi_callback_info info)
{
    napi_value ret;
    size_t argc = 1;
    napi_value argv[1];

    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    RBACConfiguration *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, argv[0], &type));
    if (type == napi_string)
    {
        NAPI_CALL(
            NAPIUtils::getUTF8String(env, obj->m_applicationName, argv[0]));
        obj->m_cfgNeeded = true;
    }
    else
    {
        NAPI_CALL(napi_throw_type_error(env, "EINVAL", "Bad argument"));
    }

    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value RBACConfiguration::GetApplicationName(napi_env env,
                                                 napi_callback_info info)
{
    napi_value ret;
    size_t argc = 1;
    napi_value argv[1];

    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    RBACConfiguration *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));

    NAPI_CALL(napi_create_string_utf8(env, obj->m_applicationName.c_str(),
                                      NAPI_AUTO_LENGTH, &ret));
    return ret;
}

napi_value RBACConfiguration::SetAuthDoneCB(napi_env env,
                                            napi_callback_info info)
{
    size_t argc = 1;
    napi_value argv[1];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    RBACConfiguration *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, argv[0], &type));
    if (type == napi_undefined || type == napi_null || type == napi_function)
    {
        if (obj->m_authDoneJSFunc)
            NAPI_CALL(napi_delete_reference(env, obj->m_authDoneJSFunc));
        if (type == napi_function)
        {
            NAPI_CALL(
                napi_create_reference(env, argv[0], 1, &obj->m_authDoneJSFunc));
        }
    }
    napi_value ret;
    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value RBACConfiguration::SetAuthErrorCB(napi_env env,
                                             napi_callback_info info)
{
    size_t argc = 1;
    napi_value argv[1];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    RBACConfiguration *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, argv[0], &type));
    if (type == napi_undefined || type == napi_null || type == napi_function)
    {
        if (obj->m_authErrorJSFunc)
            NAPI_CALL(napi_delete_reference(env, obj->m_authErrorJSFunc));
        if (type == napi_function)
        {
            NAPI_CALL(napi_create_reference(env, argv[0], 1,
                                            &obj->m_authErrorJSFunc));
        }
    }
    napi_value ret;
    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

napi_value RBACConfiguration::SetAuthExpiredCB(napi_env env,
                                               napi_callback_info info)
{
    size_t argc = 1;
    napi_value argv[1];
    napi_value jsthis;
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, &jsthis, nullptr));

    RBACConfiguration *obj;
    NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
    napi_valuetype type;
    NAPI_CALL(napi_typeof(env, argv[0], &type));
    if (type == napi_undefined || type == napi_null || type == napi_function)
    {
        if (obj->m_authExpiredJSFunc)
            NAPI_CALL(napi_delete_reference(env, obj->m_authExpiredJSFunc));
        if (type == napi_function)
        {
            NAPI_CALL(napi_create_reference(env, argv[0], 1,
                                            &obj->m_authExpiredJSFunc));
        }
    }
    napi_value ret;
    NAPI_CALL(napi_get_undefined(env, &ret));
    return ret;
}

cmw::rda3::client::ClientService *RBACConfiguration::getClientService()
{
    if (m_cfgNeeded)
        reconfigure();
    return m_clientSvc.get();
}

void RBACConfiguration::authChangedJSReplyCallback(napi_env env,
                                                   napi_value /* js_callback */,
                                                   void *context,
                                                   void *vdata)
{
    napi_ref wrapper = reinterpret_cast<napi_ref>(context);
    RBACAuthNotification *data = reinterpret_cast<RBACAuthNotification *>(vdata);

    napi_status status;
    napi_value jsthis;
    status = napi_get_reference_value(env, wrapper, &jsthis);
    if ((status == napi_ok) && (jsthis != nullptr))
    {
        RBACConfiguration *obj;
        NAPI_CALL(napi_unwrap(env, jsthis, reinterpret_cast<void **>(&obj)));
        napi_value arg = nullptr;
        napi_value func = nullptr;

        if (data->newToken && obj->m_authDoneJSFunc)
        {
            // Create a token
            arg = createToken(env, *data->newToken);
            NAPI_CALL(
                napi_get_reference_value(env, obj->m_authDoneJSFunc, &func));
        }
        else if (!data->errMsg.empty() && obj->m_authErrorJSFunc)
        {
            // Create a token
            NAPI_CALL(napi_create_string_utf8(env, data->errMsg.c_str(),
                                              NAPI_AUTO_LENGTH, &arg));
            NAPI_CALL(
                napi_get_reference_value(env, obj->m_authErrorJSFunc, &func));
        }
        else if (data->oldToken && obj->m_authExpiredJSFunc)
        {
            arg = createToken(env, *data->oldToken);
            NAPI_CALL(
                napi_get_reference_value(env, obj->m_authExpiredJSFunc, &func));
        }
        if (func)
        {
            napi_value ret;
            status = napi_call_function(env, jsthis, func, 1, &arg, &ret);
            if (status == napi_pending_exception)
            {
                napi_value ex;
                NAPI_CALL(napi_get_and_clear_last_exception(env, &ex));
                bool isError;
                NAPI_CALL(napi_is_error(env, ex, &isError));
                if (isError)
                {
                    napi_value msg;
                    NAPI_CALL(napi_get_named_property(env, ex, "message", &msg));
                    std::string msgStr;
                    NAPIUtils::getUTF8String(env, msgStr, msg);
                    std::cout << "Error text :" << msgStr << std::endl;
                    NAPI_CALL(napi_throw(env, ex));
                }
            }
        }
    }
    delete data;
}

napi_value RBACConfiguration::createToken(napi_env env, const rbac::Token &token)
{
    napi_value ret;
    NAPI_CALL(napi_create_object(env, &ret));

    napi_value signValid;
    NAPI_CALL(napi_get_boolean(env, token.isSignatureVerified(), &signValid));
    NAPI_CALL(napi_set_named_property(env, ret, "signatureVerified", signValid));

    napi_value tokenExp;
    NAPI_CALL(napi_get_boolean(env, token.isTokenExpired(), &tokenExp));
    NAPI_CALL(napi_set_named_property(env, ret, "tokenExpired", tokenExp));

    napi_value serialID;
    NAPI_CALL(napi_create_int32(env, token.getSerialID(), &serialID));
    NAPI_CALL(napi_set_named_property(env, ret, "serialID", serialID));

    napi_value authTime;
    NAPI_CALL(napi_create_int64(env, token.getAuthenticationTime(), &authTime));
    NAPI_CALL(napi_set_named_property(env, ret, "authenticationTime", authTime));

    napi_value expTime;
    NAPI_CALL(napi_create_int64(env, token.getExpirationTime(), &expTime));
    NAPI_CALL(napi_set_named_property(env, ret, "expirationTime", expTime));

    napi_value appName;
    NAPI_CALL(napi_create_string_utf8(env, token.getApplicationName().c_str(),
                                      NAPI_AUTO_LENGTH, &appName));
    NAPI_CALL(napi_set_named_property(env, ret, "applicationName", appName));

    napi_value locName;
    NAPI_CALL(napi_create_string_utf8(env, token.getLocationName().c_str(),
                                      NAPI_AUTO_LENGTH, &locName));
    NAPI_CALL(napi_set_named_property(env, ret, "locationName", locName));

    napi_value userName;
    NAPI_CALL(napi_create_string_utf8(env, token.getUserName().c_str(),
                                      NAPI_AUTO_LENGTH, &userName));
    NAPI_CALL(napi_set_named_property(env, ret, "userName", userName));

    std::vector<std::string> rolesVect = token.getRoles();
    napi_value roles;
    NAPI_CALL(napi_create_array_with_length(env, rolesVect.size(), &roles));
    for (size_t i = 0; i < rolesVect.size(); ++i)
    {
        napi_value role;
        NAPI_CALL(napi_create_string_utf8(env, rolesVect[i].c_str(),
                                          NAPI_AUTO_LENGTH, &role));
        NAPI_CALL(napi_set_element(env, roles, i, role));
    }
    NAPI_CALL(napi_set_named_property(env, ret, "roles", roles));

    napi_value fullName;
    NAPI_CALL(napi_create_string_utf8(env, token.getFullName().c_str(),
                                      NAPI_AUTO_LENGTH, &fullName));
    NAPI_CALL(napi_set_named_property(env, ret, "fullName", fullName));

    napi_value email;
    NAPI_CALL(napi_create_string_utf8(env, token.getUserEmail().c_str(),
                                      NAPI_AUTO_LENGTH, &email));
    NAPI_CALL(napi_set_named_property(env, ret, "email", email));

    napi_value accountType;
    NAPI_CALL(napi_create_string_utf8(
        env, rbac::accountTypetoString(token.getAccountType()).c_str(),
        NAPI_AUTO_LENGTH, &accountType));
    NAPI_CALL(napi_set_named_property(env, ret, "accountType", accountType));

    return ret;
}
