#/bin/sh
apt-get update
apt-get install -y python3 build-essential
apt-get install -y apt-transport-https
echo 'deb [trusted=yes] https://mro-dev.web.cern.ch/distfiles/debian/stretch mro-main/' >> /etc/apt/sources.list
apt-get update
apt-get install -y cmw-core
echo "search cern.ch" >> /etc/resolv.conf
npm install bindings

