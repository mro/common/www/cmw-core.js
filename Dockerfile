FROM node:lts-slim

RUN apt-get update \
    && apt-get install -y python3 build-essential apt-transport-https \
    && apt-get install -y git libcurl4-nss-dev \
    && echo 'deb [trusted=yes] https://mro-dev.web.cern.ch/distfiles/debian/stretch mro-main/' >> /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y cmw-core \
    && apt-get clean \
    && echo "search cern.ch" >> /etc/resolv.conf
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:node_modules/.bin

ENTRYPOINT /bin/bash
