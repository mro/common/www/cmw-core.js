#/bin/sh
apt-get update
apt-get install -y python3 build-essential
apt-get install -y apt-transport-https
echo 'deb [trusted=yes] https://apc-dev.web.cern.ch/distfiles/debian/stretch apc-main/' >> /etc/apt/sources.list
apt-get update
apt-get install -y cmw-core libcurl4-nss-dev
echo "search cern.ch" >> /etc/resolv.conf
npm install bindings
mkdir -p /user/rbac/pkey
cp /d/Users/mdonze/Projects/rbac/pkey/* /user/rbac/pkey
