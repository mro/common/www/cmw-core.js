{
  "targets": [
    {
      "target_name": "RDA3",
      "sources": [
        "src/RDA3.cpp",
        "src/AccessPoint.cpp",
        "src/Data.cpp",
        "src/RBACConfiguration.cpp",
        "src/AcquiredData.cpp",
        "src/AsyncGetter.cpp",
        "src/AsyncSetter.cpp",
        "src/NAPIUtils.cpp",
        "src/Server.cpp"
      ],
      'cflags!': [ '-fno-exceptions', '-fno-rtti' ],
      'cflags_cc!': [ '-fno-exceptions', '-fno-rtti' ],
      'ldflags' : [
          '-fPIC',
          '-Wl,-rpath,/opt/cmw-core/lib',
          '-Wl,-rpath,/opt/cmw-core/lib64'
      ],
      'include_dirs': ['/opt/cmw-core/include', '/opt/cmw-core/include/cmw-core'],
      'link_settings': {
        'libraries': [
          '-lcmw-fwk', '-lcmw-rda3', '-lcmw-rbac', '-lcmw-directory-client',
          '-lcmw-data', '-lcmw-util', '-lcmw-log', '-lzmq', '-lboost_date_time',
          '-lboost_program_options', '-lboost_atomic', '-lboost_filesystem',
          '-lboost_regex', '-lboost_log', '-lboost_system', '-lboost_chrono',
          '-lboost_log_setup', '-lboost_thread', '-lpthread', '-lcurl'
        ],
        'library_dirs': ['/opt/cmw-core/lib','/opt/cmw-core/lib64']
      },
      'dependencies': [],
      'conditions': [
        ['OS=="win"', {
          "msvs_settings": {
            "VCCLCompilerTool": {
              "ExceptionHandling": 1
            }
          }
        }],
        ['OS=="mac"', {
          "xcode_settings": {
            "CLANG_CXX_LIBRARY": "libc++",
            'GCC_ENABLE_CPP_EXCEPTIONS': 'YES',
            'MACOSX_DEPLOYMENT_TARGET': '10.7'
          }
        }]
      ]
    }
  ]
}
