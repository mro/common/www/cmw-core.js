const { Data, Server, AccessPoint, AcquiredData } = require('.');
const util = require('util');

var subSrc = undefined;
var i = 0;
var notify = function(){
    if(subSrc != undefined) {
        console.log('Notifying');
        var d = new AcquiredData();
        d.cycleName = "MyCycle";
        d.cycleStamp = BigInt(0);
        d.acqStamp = BigInt(0);
        d.data = new Data();
        d.data.append('test', i);
        subSrc.notify(d);
        ++i;
        setTimeout(notify, 100);
    }
};

var ap = new AccessPoint('TheDevice', 'TheProperty', 
                                'rda3test.localhost');
var server = new Server('rda3test.localhost');
server.setSubscribeCallback(function(req){
    console.log('======== Subscribe ==========');
    console.log(req);
    var d = new AcquiredData();
    d.cycleName = "MyCycle";
    d.cycleStamp = BigInt(0);
    d.acqStamp = BigInt(0);
    d.data = new Data();
    d.data.append('test', 'Hello new user!');
    return d;
});
server.setUnsubscribeCallback(function(req){
    console.log('======== Unsubscribe ==========');
    console.log(req);
});

server.setSubSrcAddedCallback(function(req){
    console.log('======== Subscription source added ==========');
    console.log(req);
    subSrc = req;
});


server.setSubSrcRemovedCallback(function(req){
    console.log('======== Subscription source removed ==========');
    console.log(req);
    //subSrc = undefined;
});
server.start();

ap.on('data', function(d){
    console.log('======== Data received ==========');
    console.log(d.data.entries);
});
ap.on('error', function(err){
    console.log(err);
});
ap.subscribe();

setTimeout(()=>notify(),1000);

setTimeout(function(){
    ap.unsubscribe();
    setTimeout(()=>server.stop(), 1000);
}, 15000);
