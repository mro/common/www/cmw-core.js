'use strict'

const EventEmitter = require('events').EventEmitter
const RDA3 = require('bindings')('RDA3')
const inherits = require('util').inherits
const util = require('util')
const colors = require('colors')

inherits(RDA3.AccessPoint, EventEmitter);

module.exports = RDA3;

var rbc = new RDA3.RBACConfiguration();
var ap = new RDA3.AccessPoint('TCDIV.20607', 'ExpertSetting');
rbc.setLoginPolicy('EXPLICIT');
rbc.setLoginName('mdonze');
var authdone = function(token) {
    console.log(util.inspect(token, false, null, true));
};
rbc.authenticationDone = authdone;

var autherror = function(error) {
    console.log(error.red);
};
rbc.authenticationError = autherror;

var doget = function(){
    ap.setRBACConfiguration(rbc);
    ap.get().then(function(d){
            console.log(`==> Got : ${d.data.entries.acquisitionDelayMDC}`.green)})
        .catch(function(e){
            console.log(`Error : ${e}`.brightRed)});
};
