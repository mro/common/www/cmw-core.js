var chai = require('chai');
const
  { expect } = chai,
  { describe, before, after, it } = require('mocha'),
  chaiAsPromised = require('chai-as-promised');

const { Data, Server, AccessPoint } = require('..');

chai.use(chaiAsPromised);

describe('RDA3Server', function() {
  var server;

  before(function() {
    AccessPoint.reset();
    server = new Server('rda3test.localhost');
    server.start();
  });
  after(function() {
    server.stop();
    server = undefined;
  });

  describe('#setGetCallback()', function() {
    it("Call get callback", function() {
      server.setGetCallback(function(req) {
        expect(req.AccessPoint).to.equal('TheDevice/TheProperty');
        expect(req.Device).to.equal('TheDevice');
        expect(req.Property).to.equal('TheProperty');
        const data = new Data();
        data.append("test", 42);
        return data;
      });
      var ap = new AccessPoint('TheDevice', 'TheProperty',
        'rda3test.localhost');
      return ap.get()
      .then(function(data) {
        expect(data.data.entries.test).to.equal(42);
      }).finally(function() {
        server.setGetCallback();
      });
    });
  });
  describe('#setSetCallback()', function() {
    it("Call set callback", function(done) {
      server.setSetCallback(function(req) {
        expect(req.Data.entries.test).to.equal('Set from JS!');
        server.setSetCallback();
        done();
      });
      var ap = new AccessPoint('TheDevice', 'TheProperty',
        'rda3test.localhost');
      const data = new Data();
      data.append('test', 'Set from JS!');
      ap.set(data);
    });
    it('Reject set using exception', async function() {
      server.setSetCallback(function() {
        throw new Error('Set rejected');
      });
      var ap = new AccessPoint('TheDevice', 'TheProperty',
        'rda3test.localhost');
      const data = new Data();
      await expect(ap.set(data)).to.be.rejectedWith('rejected');
    });
    it('Reject set returning error message', async function() {
      server.setSetCallback(function() {
        return 'Set rejected';
      });
      var ap = new AccessPoint('TheDevice', 'TheProperty',
        'rda3test.localhost');
      const data = new Data();
      await expect(ap.set(data)).to.be.rejectedWith('rejected');
    });
  });
  describe('#setSubscribeCallback()', function() {
    var ap = new AccessPoint('TheDevice', 'TheProperty',
      'rda3test.localhost');
    it("Call subscribe callback", function(done) {
      server.setSubscribeCallback(function(req) {
        expect(req.AccessPoint).to.equal('TheDevice/TheProperty');
        expect(req.Device).to.equal('TheDevice');
        expect(req.Property).to.equal('TheProperty');
        server.setSubscribeCallback();
        ap.unsubscribe();
        done();
      });
      ap.subscribe();
    });
  });
});
