var chai = require('chai');
const
  { expect } = chai,
  { describe, before, after, it } = require('mocha'),
  chaiAsPromised = require('chai-as-promised');

const { Data, Server, AccessPoint } = require('..');

chai.use(chaiAsPromised);

describe('RDA3Client', function() {
  /** @type {Server} */
  var server; // server used in tests

  before(function() {
    AccessPoint.reset();
    server = new Server('rda3test.localhost');
    server.start();
  });
  after(function() {
    server.stop();
    server = undefined;
  });

  it('#contextData in set requests', function(done) {
    var ap = new AccessPoint('TheDevice', 'TheProperty',
      'rda3test.localhost');
    const data = new Data();
    data.append('test', 'Set from JS!');

    server.setSetCallback(function(req) {
      expect(req.Data.entries.test).to.equal('Set from JS!');
      expect(req.Context?.Data?.entries?.user).to.equal('me');
      server.setSetCallback();
      done();
    });

    const contextData = new Data();
    contextData.append('user', 'me');
    ap.set(data, null, contextData);
  });

  it('#contextData in get requests', function(done) {
    var ap = new AccessPoint('TheDevice', 'TheProperty',
      'rda3test.localhost');

    server.setGetCallback(function(req) {
      expect(req.Context?.Data?.entries?.user).to.equal('me');
      server.setGetCallback();
      done();
    });

    const contextData = new Data();
    contextData.append('user', 'me');
    ap.get(null, contextData);
  });

  it('#contextData in subscribe requests', function(done) {
    var ap = new AccessPoint('TheDevice', 'TheProperty',
      'rda3test.localhost');

    server.setSubscribeCallback(function(req) {
      expect(req.Context?.Data?.entries?.user).to.equal('me');
      server.setSubscribeCallback();
      done();
    });

    const contextData = new Data();
    contextData.append('user', 'me');
    ap.subscribe(null, contextData);
    ap.unsubscribe();
  });

});
