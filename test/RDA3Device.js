// @ts-check

const
  { forEach, unset, size } = require('lodash'),
  RDA3 = require('..'),
  debug = require('debug')('test:stub');

class RDA3Device {
  /**
   * @param {string} name
   */
  constructor(name, interval = 1000) {
    this.srv = new RDA3.Server(name);
    this.count = 0;

    this.data = new RDA3.AcquiredData();
    this.data.cycleName = "MyCycle";
    this.data.cycleStamp = BigInt(0);
    this.data.acqStamp = BigInt(0);
    this.data.data = new RDA3.Data();
    this.data.data.append('msg', this.count);

    this.listeners = {};

    this.srv.setSubscribeCallback(() => {
      debug('client subscribed');
      return this.data;
    });
    this.srv.setSubSrcAddedCallback((/** @type {any} */ req) => {
      debug('adding sub %i', req.Id);
      this.listeners[req.Id] = req;
    });
    this.srv.setSubSrcRemovedCallback((/** @type {any} */ req) => {
      debug('removing sub %i', req.Id);
      unset(this.listeners, req.Id);
    });
    this.srv.setGetCallback(() => {
      return this.data.data;
    });
    this.loop = setInterval(() => this.onTick(), interval);
    debug('starting device');
    this.srv.start();
  }

  release() {
    debug('stopping device');
    this.srv.stop();
    clearInterval(this.loop);
  }

  onTick() {
    this.data.data.remove('msg');
    this.data.data.append('msg', ++this.count);
    debug('sending %i notifications', size(this.listeners));
    // @ts-ignore
    forEach(this.listeners, (l) => l.notify(this.data));
  }
}

// @ts-ignore
if (require.main === module) {
  // @ts-ignore
  const device = new RDA3Device(process.argv[2] || 'test-device.localhost'); /* jshint unused:false */
}
else {
  module.exports = RDA3Device;
}
