const
  _ = require('lodash'),
  { expect } = require('chai'),
  { describe, it } = require('mocha');

const { Data } = require('../');

describe('RDA3Data', function() {
  describe('#append()', function() {
    [
      {
        name: 'append scalar values',
        default: 42,
        values: [
          { },
          { type: Data.NumberType.Int8 },
          { type: Data.NumberType.Int16 },
          { type: Data.NumberType.Int32 },
          { type: Data.NumberType.Int64, result: BigInt(42) },
          { type: Data.NumberType.Float },
          { type: Data.NumberType.Double },
          { set: 'hello' },
          { set: true }
        ]
      },
      {
        name: 'append number values from string',
        default: '42',
        values: [
          { type: Data.NumberType.Int8, result: 42 },
          { type: Data.NumberType.Int16, result: 42 },
          { type: Data.NumberType.Int32, result: 42 },
          { type: Data.NumberType.Int64, result: BigInt(42) },
          { type: Data.NumberType.Float, result: 42 },
          { type: Data.NumberType.Double, result: 42 }
        ]
      },
      {
        name: 'append array values',
        default: [ 0, 1 ],
        values: [
          { },
          { type: Data.NumberType.Int8 },
          { type: Data.NumberType.Int16 },
          { type: Data.NumberType.Int32 },
          { type: Data.NumberType.Int64,
            result: [ BigInt(0), BigInt(1) ]
          },
          { type: Data.NumberType.Float },
          { type: Data.NumberType.Double },
          { set: [ 'toto', 'tutu' ] },
          { set: [ true, false, true ] }
        ]
      },
      {
        name: 'append array number from string',
        default: [ '42', '53' ],
        values: [
          { type: Data.NumberType.Int8, result: [ 42, 53 ] },
          { type: Data.NumberType.Int16, result: [ 42, 53 ] },
          { type: Data.NumberType.Int32, result: [ 42, 53 ] },
          { type: Data.NumberType.Int64,
            result: [ BigInt(42), BigInt(53) ]
          },
          { type: Data.NumberType.Float, result: [ 42, 53 ] },
          { type: Data.NumberType.Double, result: [ 42, 53 ] }
        ]
      },
      {
        name: 'append 2D array values',
        default: [ [ 0, 1 ], [ 2, 3 ] ],
        values: [
          { },
          { type: Data.NumberType.Int8 },
          { type: Data.NumberType.Int16 },
          { type: Data.NumberType.Int32 },
          { type: Data.NumberType.Int64,
            result: [ [ BigInt(0), BigInt(1) ],
              [ BigInt(2), BigInt(3) ] ]
          },
          { type: Data.NumberType.Float },
          { type: Data.NumberType.Double },
          { set: [ [ 'toto', 'tutu' ], [ 'lala', 'pwet' ] ] },
          { set: [ [ true, false, true ], [ false, true, false ] ] }
        ]
      },
      {
        name: 'append 2D array number from string',
        default: [ [ '0', '1' ], [ '2', '3' ] ],
        values: [
          { type: Data.NumberType.Int8, result: [ [ 0, 1 ], [ 2, 3 ] ] },
          { type: Data.NumberType.Int16, result: [ [ 0, 1 ], [ 2, 3 ] ] },
          { type: Data.NumberType.Int32, result: [ [ 0, 1 ], [ 2, 3 ] ] },
          { type: Data.NumberType.Int64,
            result: [ [ BigInt(0), BigInt(1) ],
              [ BigInt(2), BigInt(3) ] ]
          },
          { type: Data.NumberType.Float, result: [ [ 0, 1 ], [ 2, 3 ] ] },
          { type: Data.NumberType.Double, result: [ [ 0, 1 ], [ 2, 3 ] ] }
        ]
      }
    ].forEach(function(test) {
      it(test.name, function() {
        test.values.forEach(function(val) {
          var typeStr = _.findKey(Data.NumberType,
            function(o) {return o === val.type;});
          const data = new Data();
          const value = _.defaultTo(val.set, test.default);
          const expected = _.defaultTo(val.result, value);
          data.append('test', value, val.type);
          expect(data.entries.test).to.deep.equal(expected);
          if (typeStr !== undefined) {
            // Sets number type using a string
            data.append('test1', value, typeStr.toUpperCase());
            expect(data.entries.test1).to.deep.equal(expected);
          }
        });
      });
    });
    it("Append a function", function() {
      const data = new Data();
      const func = [ { x: 0, y: 1 }, { x: 2, y: 3 } ];
      data.append("test", func);
      expect(data.entries.test).to.deep.equal(func);
    });
    it("Can't parse number from string", function() {
      const data = new Data();
      expect(function() {
        data.append('test', 'notanumber', Data.NumberType.Int32);
      }).to.throw('parse');
      expect(function() {
        data.append('test', 'notanumber', Data.NumberType.Double);
      }).to.throw('parse');
      expect(function() {
        data.append('test', [ 'notanumber' ], Data.NumberType.Int32);
      }).to.throw('parse');
      expect(function() {
        data.append('test', [ 'notanumber' ], Data.NumberType.Double);
      }).to.throw('parse');
      expect(function() {
        data.append('test', [ [ 'pun' ], [ 'pun' ] ], Data.NumberType.Int32);
      }).to.throw('parse');
      expect(function() {
        data.append('test', [ [ 'pun' ], [ 'pun' ] ], Data.NumberType.Double);
      }).to.throw('parse');
    });
  });
  describe('#setBigIntMode()', function() {
    [
      {
        type: Data.BigIntOutputType.BigInt,
        value: 42,
        expect: BigInt(42)
      },
      {
        type: Data.BigIntOutputType.String,
        value: 42,
        expect: '42'
      },
      {
        type: Data.BigIntOutputType.Timestamp,
        value: 42,
        expect: { ms: 0, ns: 42 }
      }
    ].forEach(function(test) {
      var typeStr = _.findKey(Data.BigIntOutputType,
        function(o) {return o === test.type;});
      it(`set big int mode (${typeStr})`, function() {
        const data = new Data();
        data.setBigIntMode(test.type);
        expect(data.getBigIntMode()).to.equal(test.type);

        data.setBigIntMode(typeStr);
        expect(data.getBigIntMode('')).to.equal(typeStr);

        data.append("test", test.value, Data.NumberType.Int64);
        expect(data.entries.test).to.eql(test.expect);
      });
    });

  });
  describe('#setNumberArrayMode()', function() {
    [
      {
        type: Data.NumberArrayOutputType.Regular,
        value: [ 0, 1 ],
        expect: [ 0, 1 ]
      },
      {
        type: Data.NumberArrayOutputType.TypedArray,
        value: [ 0, 1 ],
        expect: new Int32Array([ 0, 1 ])
      }
    ].forEach(function(test) {
      var typeStr = _.findKey(Data.NumberArrayOutputType,
        function(o) {return o === test.type;});
      it(`set number array mode (${typeStr})`, function() {
        const data = new Data();
        data.setNumberArrayMode(test.type);
        expect(data.getNumberArrayMode()).to.equal(test.type);

        data.setNumberArrayMode(typeStr);
        expect(data.getNumberArrayMode('')).to.equal(typeStr);

        data.append("test", test.value, Data.NumberType.Int32);
        expect(data.entries.test).to.eql(test.expect);
      });
    });
  });
  describe('#remove()', function() {
    it("removes entry", function() {
      const data = new Data();
      data.append('Test', 42);
      data.append('Test2', 43);
      expect(data.entries).to.eql({ Test: 42, Test2: 43 });
      data.remove("Test");
      expect(data.entries).to.eql({ Test2: 43 });
    });
  });
  describe('#clear()', function() {
    it("Clear entries", function() {
      const data = new Data();
      data.append('Test', 42);
      data.append('Test2', 43);
      expect(data.entries).to.eql({ Test: 42, Test2: 43 });
      data.clear();
      expect(data.entries).to.be.empty();
    });
  });
});
