
const
  { get, invoke } = require('lodash'),
  { expect } = require('chai'),
  // { fork } = require('child_process'),
  debug = require('debug')('test'),
  { makeDeferred } = require('@cern/prom'),
  { describe, it, afterEach } = require('mocha');

const
  RDA3Device = require('./RDA3Device'),
  { AccessPoint } = require('..');

describe('RDA3AccessPoint', function() {
  /**
   * @type {{
   *  device?: RDA3Device,
   *  ap?: AccessPoint,
   *  interval?: NodeJS.Interval
   * }}
   */
  var env = {};
  var id = 0;

  afterEach(function() {
    invoke(env, [ 'device', 'release' ]);
    env.device = null;

    invoke(env, [ 'ap', 'unsubscribe' ]);
    invoke(env, [ 'ap', 'removeAllListeners' ]);
    env.ap = null;
    clearInterval(env.interval);
    env.interval = null;
  });

  it('can get a value', async function() {
    this.timeout(120000);
    env.device = new RDA3Device(`cmw-core-js-test${++id}.localhost`);

    env.ap = new AccessPoint('CMW_CORE_JS_TEST_DEVICE', 'TheProperty', `cmw-core-js-test${id}.localhost`);
    const ret = await env.ap.get();
    expect(ret).to.have.nested.property('data.entries.msg');
    expect(get(ret, 'data.entries.msg')).to.be.gte(0);
  });

  it('can subscribe a device', async function() {
    this.timeout(120000);
    env.device = new RDA3Device(`cmw-core-js-test${++id}.localhost`, 250);

    env.ap = new AccessPoint('CMW_CORE_JS_TEST_DEVICE', 'TheProperty', `cmw-core-js-test${id}.localhost`);
    const def = makeDeferred();
    var msgs = [];
    env.ap.on('data', (data) => {
      debug('data event', get(data, 'data.entries'));
      msgs.push(get(data, 'data.entries.msg'));
      if (msgs.length >= 2) { def.resolve(data); }
    });
    env.ap.subscribe();
    await def.promise;

    env.ap.unsubscribe();
    expect(msgs[1] - msgs[0]).to.be.gte(1);
  });

  it('can reconnect a lost device', async function() {
    this.timeout(120000);
    // there's an issue re-starting same server in same app, let's spawn it aside
    // var child = fork(`${__dirname}/RDA3Device`, [ `cmw-core-js-test${++id}.localhost` ]);
    // env.device = { release: () => child.kill() };
    env.device = new RDA3Device(`cmw-core-js-test${++id}.localhost`);

    env.ap = new AccessPoint('CMW_CORE_JS_TEST_DEVICE', 'TheProperty', `cmw-core-js-test${id}.localhost`);
    var hadError = false;
    const def = makeDeferred();
    env.ap.on('data', (data) => {
      debug('data event', get(data, 'data.entries'));
      if (!hadError && env.device) {
        env.ap.on('error', errCB);
        /* shutdown the device */
        env.device.release();
        env.device = null;
        debug('device released');
        /* it seems that new device is not always detected by client */
        env.interval = setInterval(() => {
          debug('restarting');
          if (env.device) {
            env.device.release();
            env.device = new RDA3Device(`cmw-core-js-test${id}.localhost`);
          }
        }, 15000);
      }
      else if (hadError) {
        /* welcome back */
        def.resolve();
      }
    });
    const errCB = (error) => {
      debug('error event', error);
      if (!env.device) {
        env.device = new RDA3Device(`cmw-core-js-test${id}.localhost`);
        // const child = fork(`${__dirname}/RDA3Device`, [ `cmw-core-js-test${id}.localhost` ]);
        // env.device = { release: () => child.kill() };
      }
      hadError = true;
    };
    env.ap.subscribe();
    await def.promise;
    clearInterval(env.interval);
    env.interval = null;
    env.ap.unsubscribe();
  });
});
